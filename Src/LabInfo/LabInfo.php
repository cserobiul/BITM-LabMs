<?php

namespace Apps\LabInfo;

use PDO;
use PDOException;

class LabInfo {

    public $id, $courseName, $labNo,$seatCapacity,$proResolution,$acStauts;
    public $processor,$ram,$brand,$os, $tableCapacity,$internetSpeed;
    public $tprocessor,$tram,$tbrand,$tos;
    public $data;
    public $username,$password,$is_active;

    public function __construct() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=lab', 'root', '');
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    // assign data into variable
    public function assign($data) {
        if (!empty($data['courseName'])) {
            $this->courseName = $data['courseName'];
        }

        if (!empty($data['labNo'])) {
            $this->labNo = $data['labNo'];
        }
        
        if (!empty($data['seatCapacity'])) {
            $this->seatCapacity = $data['seatCapacity'];
        }
        if (!empty($data['proResolution'])) {
            $this->proResolution = $data['proResolution'];
        }
        if (!empty($data['acStauts'])) {
            $this->acStauts = $data['acStauts'];
        }else{
            $this->acStauts = '0';
        }
        // student  pc ifno
        if (!empty($data['processor'])) {
            $this->processor = $data['processor'];
        }
        if (!empty($data['ram'])) {
            $this->ram = $data['ram'];
        }
        if (!empty($data['brand'])) {
            $this->brand = $data['brand'];
        }
        if (!empty($data['os'])) {
            $this->os = $data['os'];
        }

        //trainer pc info
        if (!empty($data['tprocessor'])) {
            $this->tprocessor = $data['tprocessor'];
        }
        if (!empty($data['tram'])) {
            $this->tram = $data['tram'];
        }
        if (!empty($data['tbrand'])) {
            $this->tbrand = $data['tbrand'];
        }
        if (!empty($data['tos'])) {
            $this->tos = $data['tos'];
        }
        
        if (!empty($data['tableCapacity'])) {
            $this->tableCapacity = $data['tableCapacity'];
        }
        if (!empty($data['internetSpeed'])) {
            $this->internetSpeed = $data['internetSpeed'];
        }

        // login
        if (!empty($data['username'])) {
            $this->username = $data['username'];
        }
        if (!empty($data['password'])) {
            $this->password = $data['password'];
        }

        

        return $this;
    }

// get total number of lab
public function getTotalLabNumber(){
   try {
        $Query = "SELECT lab_no FROM labinfo";
        $q = $this->conn->query($Query);
        $rowCount = $q->rowCount();
         return $rowCount;
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    return $this;
}

    // get lab list
    public function getLabList() {
        try {
            $Query = "SELECT * FROM labinfo WHERE course_id = :cid";
            $q = $this->conn->prepare($Query);
            $q->execute(array(
                         ':cid' => $this->courseName
                     ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $this->data[] = $row;
                }
                return $this->data;
            } else {
                return $rowCount;
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        return $this;
    }


     // update lab info
     public function update() {
 
         try {
             $upQry = "UPDATE labinfo SET 
                         course_id = :cid,
                         seat_capacity = :sc,
                         projector_resolution = :pr,
                         ac_status = :acs,
                         pc_configuration = :pcc,
                         trainer_pc_configuration = :tpcc,
                         table_capacity = :tc,
                         internet_speed = :is,
                         updated = :updated 
                         WHERE lab_no = :labNo";
             $q = $this->conn->prepare($upQry);

              $pcc = array(
                'processor' => $this->processor,
                'ram' => $this->ram,
                'brand' => $this->brand,
                'os' => $this->os
                );
            $pc_configuration = serialize($pcc);

            $tpcc = array(
                'processor' => $this->tprocessor,
                'ram' => $this->tram,
                'brand' => $this->tbrand,
                'os' => $this->tos
                );
            $trainer_pc_configuration = serialize($tpcc);

             $q->execute(array(
                         ':cid' => $this->courseName,
                         ':sc' => $this->seatCapacity,
                         ':pr' => $this->proResolution,
                         ':acs' => $this->acStauts,
                         ':pcc' => $pc_configuration,
                         ':tpcc' => $trainer_pc_configuration,
                         ':tc' => $this->tableCapacity,
                         ':is' => $this->internetSpeed,
                         ':updated' => date("Y-m-d G:i:s"),
                         ':labNo' => $this->labNo
                     ));
 
             $rows = $q->rowCount();
             return $rows;
         } catch (Exception $ex) {
             echo 'Error: ' . $ex->getMessage();
         }
 
         return $this;
     }


    // delete form database
     public function delete() {
         try {
             $delQry = "DELETE FROM labinfo WHERE lab_no = :id";
             $q = $this->conn->prepare($delQry);
             $q->bindParam(':id', $this->labNo);
             $q->execute();
             $rows = $q->rowCount();
             return $rows;
         } catch (Exception $ex) {
             echo 'Error: ' . $ex->getMessage();
         }
     }

    // lab show function
    public function show()    {
        try {
            $loginQuery = "SELECT * FROM labinfo WHERE lab_no = :labNo ";
            $q = $this->conn->prepare($loginQuery);
            $q->execute(array(
                ':labNo' => $this->labNo,
            ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $row = $q->fetch(PDO::FETCH_ASSOC);
                return $row;
            } else {
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }

    // get all data from database
    public function index() {
        try {
            $qry = "SELECT * FROM labinfo";
            $q = $this->conn->query($qry) or die(mysql_error());
            $rows = $q->rowCount();
            if($rows>0){
                while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                    $this->data[] = $row;
                }
                return $this->data;
            }else{
                return $rows;
            }
            
        } catch (Exception $ex) {
            echo 'Error: ' . $ex->getMessage();
        }
        

        return $this;
    }

    // add lab (store)
    public function store() {    

        // if (!empty($this->courseName) && !empty($this->labNo) && !empty($this->seatCapacity)) {
        try {
            $query = "INSERT INTO labinfo (course_id, lab_no, seat_capacity, projector_resolution, ac_status, pc_configuration,  trainer_pc_configuration, table_capacity, internet_speed, created) 
            VALUES(:cid,:labno,:sc,:pr,:acs,:pcc,:tpcc,:tc,:is,:created)";
            $q = $this->conn->prepare($query);

            $pcc = array(
                'processor' => $this->processor,
                'ram' => $this->ram,
                'brand' => $this->brand,
                'os' => $this->os
                );
            $pc_configuration = serialize($pcc);

            $tpcc = array(
                'processor' => $this->tprocessor,
                'ram' => $this->tram,
                'brand' => $this->tbrand,
                'os' => $this->tos
                );
            $trainer_pc_configuration = serialize($tpcc);

            $q->execute(array(
                ':cid' => $this->courseName,
                ':labno' => $this->labNo,
                ':sc' => $this->seatCapacity,
                ':pr' => $this->proResolution,
                ':acs' => $this->acStauts,
                ':pcc' => $pc_configuration,
                ':tpcc' => $trainer_pc_configuration,
                ':tc' => $this->tableCapacity,
                ':is' => $this->internetSpeed,
                ':created' => date("Y-m-d G:i:s"),
                ));
            $rows = $q->rowCount();
            return $rows;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        // } else {
        //     echo '<script type="text/javascript">location.replace("?p=addLab");</script>';
        // }
        return $this;
    }


   



    public function chkLabNo() {
        try {
            $chkQuery = "SELECT * FROM labinfo WHERE lab_no = :lab ";
            $q = $this->conn->prepare($chkQuery);
            $q->execute(array(
                ':lab' => $this->labNo,
                ));
            $rowCount = $q->rowCount();
            return $rowCount;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }


    // user login
    public function login()
    {
        try {
            $loginQuery = "SELECT * FROM users WHERE (username = :u || email = :u) && password = :p && is_active = :ac ";
            $q = $this->conn->prepare($loginQuery);
            $q->execute(array(
                ':u' => $this->username,
                ':p' => $this->password,
                ':ac' => '1',
            ));
            $rowCount = $q->rowCount();
            if ($rowCount > 0) {
                $row = $q->fetch(PDO::FETCH_ASSOC);
                return $row;
            } else {
                return $rowCount;
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        return $this;
    }

}
