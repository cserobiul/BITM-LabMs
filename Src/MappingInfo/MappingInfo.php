<?php

namespace Apps\MappingInfo;

use PDO;
use PDOException;

class MappingInfo {

  public $id, $course_id, $batch_no,$lead_trainer,$asst_trainer,$lab_asst,$lab_id;
  public $start_date,$ending_date,$start_time,$ending_time;
  public $day;
  public $data;
  public $assigned_by;

  public function __construct() {
    if (session_status() == PHP_SESSION_NONE) {
      session_start();
    }
    try {
      $this->conn = new PDO('mysql:host=localhost;dbname=lab', 'root', '');
    } catch (PDOException $e) {
      echo 'Error: ' . $e->getMessage();
    }
  }

    // assign data into variable
  public function assign($data) {
    if (!empty($data['courseName'])) {
      $this->course_id = $data['courseName'];
    }

    if (!empty($data['labNo'])) {
      $this->lab_id = $data['labNo'];
    }

    if (!empty($data['lead_trainer'])) {
      $this->lead_trainer = $data['lead_trainer'];
    }
    if (!empty($data['asst_trainer'])) {
      $this->asst_trainer = $data['asst_trainer'];
    }
    if (!empty($data['lab_asst'])) {
      $this->lab_asst = $data['lab_asst'];
    }


    if (!empty($data['batch_no'])) {
      $this->batch_no = $data['batch_no'];
    }
    if (!empty($data['start_date'])) {            
      $originalDate = $data['start_date'];
      $newDate = date("Y-m-d", strtotime($originalDate));
      $this->start_date = $newDate;
    }

    if (!empty($data['ending_date'])) {
      $ending_date = $data['ending_date'];
      $ending_date = date("Y-m-d", strtotime($ending_date));
      $this->ending_date = $ending_date;
    }



    if (!empty($data['start_time'])) {
      $this->start_time = $data['start_time'];
    }
    if (!empty($data['ending_time'])) {
      $this->ending_time = $data['ending_time'];
    }
    if (!empty($data['day'])) {
      $this->day = $data['day'];
    }


    if (!empty($data['assigned_by'])) {
      $this->assigned_by = $_SESSION['loginUser'];
    }

    if (!empty($data['id'])) {
      $this->id = $data['id'];
    }



    return $this;
  }

  public function getLab() {
    try {
      $qry = "SELECT * FROM labinfo WHERE course_id = :cid";
      $q = $this->conn->prepare($qry);
      $q->execute(array(
        ':cid' => '84832sd8384d6d',
        ));
      $rows = $q->rowCount();
      if($rows>0){
        while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
          $this->data[] = $row;
        }
        return $this->data;
      }else{
        return $rows;
      }

    } catch (Exception $ex) {
      echo 'Error: ' . $ex->getMessage();
    }


    return $this;
  }

  
    // chk today's class schedule 
  public function todaysClass(){
    $tdate = date('Y-m-d', time() - 6*4800);
    try{
      $qry = "SELECT * FROM course_trainer_lab_mapping 
      WHERE  start_date <= :sd && ending_date >= :sd 
      ORDER BY start_time ASC";
      $q = $this->conn->prepare($qry);
      $q->execute(array(
        ':sd' => $tdate,
        ));
      $rows = $q->rowCount();
      if($rows>0){
        while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
          $this->data[] = $row;
        }
        return $this->data;
      }else{
        return $rows;
      }
    }catch (PDOException $e){
      echo 'Error: '.$e->getMessage;
    }
    return $this;

  }


    // assign courses (store)
  public function store() {    

        // if (!empty($this->courseName) && !empty($this->labNo) && !empty($this->seatCapacity)) {
    try {
      $query = "INSERT INTO course_trainer_lab_mapping (course_id, batch_no, lead_trainer, asst_trainer, lab_asst, lab_id,  start_date, ending_date, start_time,ending_time,day,assigned_by, created) 
      VALUES(:cid,:bid,:lt,:at,:la,:lid,:sd,:ed,:st,:et,:day,:assby,:created)";
      $q = $this->conn->prepare($query);

      $cdate = date('Y-m-d, H:i', time() - 6*4800);

      $q->execute(array(
        ':cid' => $this->course_id,
        ':bid' => $this->batch_no,
        ':lt' => $this->lead_trainer,
        ':at' => $this->asst_trainer,
        ':la' => $this->lab_asst,
        ':lid' => $this->lab_id,
        ':sd' => $this->start_date,
        ':ed' => $this->ending_date,
        ':st' => $this->start_time,
        ':et' => $this->ending_time,
        ':day' => $this->day,
        ':assby' => $_SESSION['loginUser'],
        ':created' => $cdate,
        ));
      $rows = $q->rowCount();
      return $rows;
    } catch (PDOException $e) {
      echo 'Error: ' . $e->getMessage();
    }
        // } else {
        //     echo '<script type="text/javascript">location.replace("?p=addLab");</script>';
        // }
    return $this;
  }



    // batch already exist ?
  public function chkBatchno(){
    try{
      $qry = "SELECT * FROM course_trainer_lab_mapping 
      WHERE course_id = :cid && batch_no = :bid ";
      $q = $this->conn->prepare($qry);
      $q->execute(array(
        ':cid' => $this->course_id,
        ':bid' => $this->batch_no,
        ));

      return $rows = $q->rowCount();
    }catch (PDOException $e){
      echo 'Error: '.$e->getMessage;
    }
    return $this;

  }




     // Lab busy ?
  public function labShedule(){
    try{
      $qry = "SELECT * FROM course_trainer_lab_mapping 
      WHERE  day = :day && lab_id = :lid && start_date <= :sd && ending_date >= :sd
      && start_time <= :st && ending_time >=:et ";
      $q = $this->conn->prepare($qry);
      $q->execute(array(
        ':sd' => $this->start_date,
        ':ed' => $this->ending_date,
        ':day' => $this->day,
        ':st' => $this->start_time,
        ':et' => $this->ending_time,
        ':lid' => $this->lab_id
        ));

      return $rows = $q->rowCount();
    }catch (PDOException $e){
      echo 'Error: '.$e->getMessage;
    }
    return $this;

  }




    // lead trainer busy ?
  public function leadTrainerShedule(){
    try{
      $qry = "SELECT * FROM course_trainer_lab_mapping 
      WHERE day = :day && lead_trainer = :lt && start_date <= :sd && ending_date >= :sd
      && start_time <= :st && ending_time >=:et ";
      $q = $this->conn->prepare($qry);
      $q->execute(array(
        ':sd' => $this->start_date,
        ':ed' => $this->ending_date,
        ':day' => $this->day,
        ':st' => $this->start_time,
        ':et' => $this->ending_time,
        ':lt' => $this->lead_trainer
        ));

      return $rows = $q->rowCount();
    }catch (PDOException $e){
      echo 'Error: '.$e->getMessage;
    }
    return $this;

  }


    // assist trainer busy ?
  public function assistTrainerShedule(){


    try{
      $qry = "SELECT * FROM course_trainer_lab_mapping 
      WHERE day = :day && asst_trainer = :ast && start_date <= :sd && ending_date >= :sd
      && start_time <= :st && ending_time >=:et ";
      $q = $this->conn->prepare($qry);
      $q->execute(array(
        ':sd' => $this->start_date,
        ':ed' => $this->ending_date,
        ':day' => $this->day,
        ':st' => $this->start_time,
        ':et' => $this->ending_time,
        ':ast' => $this->asst_trainer
        ));

      return $rows = $q->rowCount();
    }catch (PDOException $e){
      echo 'Error: '.$e->getMessage;
    }
    return $this;

  }


     // lab assistant busy ?
  public function labAsstShedule(){


    try{
      $qry = "SELECT * FROM course_trainer_lab_mapping 
      WHERE day = :day && lab_asst = :labas && start_date <= :sd && ending_date >= :sd
      && start_time <= :st && ending_time >=:et ";
      $q = $this->conn->prepare($qry);
      $q->execute(array(
        ':sd' => $this->start_date,
        ':ed' => $this->ending_date,
        ':day' => $this->day,
        ':st' => $this->start_time,
        ':et' => $this->ending_time,
        ':labas' => $this->lab_asst
        ));

      return $rows = $q->rowCount();
    }catch (PDOException $e){
      echo 'Error: '.$e->getMessage;
    }

    return $this;

  }





// get lab list
  public function getLabList() {
    try {
      $qry = "SELECT * FROM labinfo ORDER BY lab_no ASC";
      $q = $this->conn->query($qry);
      $rows = $q->rowCount();
      if($rows>0){
        while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
          $this->data[] = $row;
        }
        return $this->data;
      }else{
        return $rows;
      }

    } catch (Exception $ex) {
      echo 'Error: ' . $ex->getMessage();
    }


    return $this;
  }


     // update lab info
  public function update() {

   try {
     $upQry = "UPDATE course_trainer_lab_mapping SET 
     batch_no = :bn,
     WHERE course_id = :id";
     $q = $this->conn->prepare($upQry);
     $q->execute(array(
       ':bn' => $this->batch_no,
       ':id' => $this->id
       ));

     $rows = $q->rowCount();
     return $rows;
   } catch (Exception $ex) {
     echo 'Error: ' . $ex->getMessage();
   }

   return $this;
 }


    // delete form database
 public function trush() {
  $tdate = date('Y-m-d, H:i', time() - 6*4800);      
  try {
   $trushQry = "UPDATE course_trainer_lab_mapping SET 
   is_delete = :tid,
   updated = :updated
   WHERE id = :id";
   $q = $this->conn->prepare($trushQry);
   $q->execute(array(
     ':tid' => '1',
     ':updated' => $tdate,
     ':id' => $this->id
     ));
   $rows = $q->rowCount();
   return $rows;
 } catch (Exception $ex) {
   echo 'Error: ' . $ex->getMessage();
 }
}

    // lab show function
public function show()    {
  try {
    $loginQuery = "SELECT * FROM course_trainer_lab_mapping WHERE course_id = :cid ";
    $q = $this->conn->prepare($loginQuery);
    $q->execute(array(
      ':cid' => $this->id,
      ));
    $rowCount = $q->rowCount();
    if ($rowCount > 0) {
      $row = $q->fetch(PDO::FETCH_ASSOC);
      return $row;
    } else {
      return $rowCount;
    }

  } catch (PDOException $e) {
    echo 'Error: ' . $e->getMessage();
  }

  return $this;
}

    // get all data from database
public function index() {
  $today = date('Y-m-d', time() - 6*4800);

  try {
    $qry = "SELECT * FROM course_trainer_lab_mapping WHERE ending_date >= '$today' AND is_delete = 0 ORDER BY created DESC";
    $q = $this->conn->query($qry) or die(mysql_error());
    $rows = $q->rowCount();
    if($rows>0){
      while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
        $this->data[] = $row;
      }
      return $this->data;
    }else{
      return $rows;
    }

  } catch (Exception $ex) {
    echo 'Error: ' . $ex->getMessage();
  }


  return $this;
}




    // get course name from id

public function getCourseName(){
  try {
    $Query = "SELECT * FROM courses WHERE unique_id = :id";
    $q = $this->conn->prepare($Query);
    $q->execute(array(
      ':id' => $this->courseName,
      ));
    $rowCount = $q->rowCount();
    if ($rowCount > 0) {
      $data = $q->fetch(PDO::FETCH_ASSOC);
      return $data;
    }else{
      return $rowCount;
    }

  } catch (PDOException $e) {
    echo 'Error: ' . $e->getMessage();
  }
  return $this;
}




// get runnig courses

public function getRunningCourses(){
 try {
        //$Query = "SELECT COUNT(course_id) FROM course_trainer_lab_mapping;";
  $Query = "SELECT DISTINCT course_id FROM course_trainer_lab_mapping";
  $q = $this->conn->query($Query);
  $rowCount = $q->rowCount();
  return $rowCount;
} catch (PDOException $e) {
  echo "Error: " . $e->getMessage();
}
return $this;
}
    // get course list
public function getCourseList() {
  try {
    $Query = "SELECT * FROM courses ORDER BY title ASC";
    $q = $this->conn->query($Query);
    $rowCount = $q->rowCount();
    if ($rowCount > 0) {
      while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
        $this->data[] = $row;
      }
      return $this->data;
    } else {
      return $rowCount;
    }
  } catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  return $this;
}



}
