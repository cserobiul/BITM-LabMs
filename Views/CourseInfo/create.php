<?php
$data = $courseInfoObject->getCourseList();
?>
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Add Course</li>
    </ul>
</div>
<br>

    <div class="tab-pane">
        <a href="?p=allLab">
            <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> Show All Courses</button>
        </a> 
    </div>
     <br>

<form class="form-horizontal" method="POST" action="?p=courseStore">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Add Course Information</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Add New Course </legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Course Name:</label>
                            <div class="col-lg-9">
                                <select name="courseName" data-placeholder="Select Course Name" class="select">
                                    <option></option>

                                    <?php
                                    if (!empty($data)) {
                                        foreach ($data as $value) {
                                            ?>
                                            <option value="<?php echo $value['unique_id'] ?>">
                                                <?php echo $value['title'] ?></option>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php if(isset($_SESSION['courseNameErrMsg']) && !empty($_SESSION['courseNameErrMsg'])){ ?>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-9">
                                    <?php echo $_SESSION['courseNameErrMsg'];
                                        unset($_SESSION['courseNameErrMsg']) ?>
                                     </div>
                                </div>
                                <?php } ?>


                            <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Duration:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                    <div class="mb-15">
                                                        <select name="durationNumber" data-placeholder="Select Number" class="select">
                                                            <option></option>
                                                            <option value="1">1</option> 
                                                            <option value="2">2</option> 
                                                            <option value="3">3</option> 
                                                            <option value="4">4</option>
                                                            <option value="5">5</option> 
                                                            <option value="6">6</option> 
                                                            <option value="7">7</option> 
                                                            <option value="8">8</option>
                                                            <option value="9">9</option> 
                                                            <option value="10">10</option> 
                                                            <option value="11">11</option> 
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                            </div>
                                            <div class="col-md-6">
                                                    <div class="mb-15">
                                                        <select name="durationDay" data-placeholder="Select Day/Month" class="select">
                                                            <option></option>
                                                            <option value="Day">Day</option> 
                                                            <option value="Month">Month</option> 
                                                            <option value="Yesr">Year</option>                                                       
                                                        </select>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Description:</label>
                                    <div class="col-lg-9">
                                        <textarea name="description" class="form-control" rows="5" placeholder="Course Description"></textarea>                                   
                                    </div>
                                </div>

                                 <?php if(isset($_SESSION['seatCapacityErrMsg']) && !empty($_SESSION['seatCapacityErrMsg'])){ ?>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-9">
                                    <?php echo $_SESSION['seatCapacityErrMsg'];
                                        unset($_SESSION['seatCapacityErrMsg']) ?>
                                     </div>
                                </div>
                                <?php } ?>

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Type:</label>
                                    <div class="col-lg-9">
                                        <select name="courseType" class="select" data-placeholder="Select Course Type" >
                                            <option></option>
                                            <option value="Free">Free</option>
                                            <option value="Paid">Paid</option>
                                        </select>
                                    </div>
                                </div>
                        
                                

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Fee:</label>
                                    <div class="col-lg-9">
                                        <input name="coursefee" type="number" class="form-control" placeholder="Ex: BDT 10000">
                                    </div>
                                </div>
                        
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Status:</label>
                                    <div class="col-lg-9">
                                        <select name="courseType" class="select" data-placeholder="Select Course Status" >
                                            <option></option>
                                            <option value="Free">Open</option>
                                            <option value="Paid">Close</option>
                                        </select>
                                        <!--<input name="tableCapacity" type="number" class="form-control" placeholder="Table Capacity">-->
                                    </div>
                                </div>

<!--                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Course Status:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select onchange="acStatus(this);" name="acStauts" data-placeholder="Select AC Status" class="select">
                                                        <option></option>
                                                        <option value="yes">Yes</option> 
                                                        <option value="no">No</option> 
                                                    </select>
                                                </div>                                                
                                            </div> 

                                            <div class="col-md-6" id="acStatus" style="display: none;">
                                                <input name="acStauts" value="0" type="text" placeholder="How many AC" class="form-control mb-15">

                                            </div>

                                        </div>
                                    </div>
                                </div>-->

                            </fieldset>
                        </div>

<!--                        <div class="col-md-6">
                            <fieldset>
                                <legend class="text-semibold"> &nbsp;</legend>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Student PC Configuration:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="processor" data-placeholder="Select Processor" class="select">
                                                        <option></option>
                                                        <option value="celeron">Celeron</option> 
                                                        <option value="core 2 due">Core 2 Due</option> 
                                                        <option value="core 2 quad">Core 2 Quad</option> 
                                                        <option value="core i5">Core i5</option> 
                                                        <option value="core i7">Core i7</option> 
                                                    </select>
                                                </div>                                                                                                                       
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="ram" data-placeholder="Select RAM" class="select">
                                                        <option></option>
                                                        <option value="1 gb">1 GB</option> 
                                                        <option value="2 gb">2 GB</option> 
                                                        <option value="4 gb">4 GB</option> 
                                                        <option value="8 gb">8 GB</option> 
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="monitor" data-placeholder="Select Monitor" class="select">
                                                        <option></option>
                                                        <option value="dell">Dell</option> 
                                                        <option value="sumsang">Sumsang</option> 
                                                        <option value="lg">LG</option> 
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="os" data-placeholder="Select OS" class="select">
                                                        <option></option>
                                                        <option value="windows 7">Windows 7</option> 
                                                        <option value="windows 8">Windows 8</option> 
                                                        <option value="windows 8.1">Windows 8.1</option> 
                                                        <option value="windows 10">Windows 10</option> 
                                                        <option value="redhat linux">RedHat Linux</option> 
                                                        <option value="cent os">Cent OS Linux</option> 
                                                        <option value="mac">Mac</option> 
                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                 trainer pc config 
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Trainer PC Configuration:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="tprocessor" data-placeholder="Select Processor" class="select">
                                                        <option></option>
                                                        <option value="celeron">Celeron</option> 
                                                        <option value="core 2 due">Core 2 Due</option> 
                                                        <option value="core 2 quad">Core 2 Quad</option> 
                                                        <option value="core i5">Core i5</option> 
                                                        <option value="core i7">Core i7</option> 
                                                    </select>
                                                </div>                                                                                                                       
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="tram" data-placeholder="Select RAM" class="select">
                                                        <option></option>
                                                        <option value="1 gb">1 GB</option> 
                                                        <option value="2 gb">2 GB</option> 
                                                        <option value="4 gb">4 GB</option> 
                                                        <option value="8 gb">8 GB</option> 
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="tmonitor" data-placeholder="Select Monitor" class="select">
                                                        <option></option>
                                                        <option value="dell">Dell</option> 
                                                        <option value="sumsang">Sumsang</option> 
                                                        <option value="lg">LG</option> 
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="tos" data-placeholder="Select OS" class="select">
                                                        <option></option>
                                                        <option value="windows 7">Windows 7</option> 
                                                        <option value="windows 8">Windows 8</option> 
                                                        <option value="windows 8.1">Windows 8.1</option> 
                                                        <option value="windows 10">Windows 10</option> 
                                                        <option value="redhat linux">RedHat Linux</option> 
                                                        <option value="cent os">Cent OS Linux</option> 
                                                        <option value="mac">Mac</option> 
                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Internet Speed:</label>
                                    <div class="col-lg-9">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="mb-15">
                                                    <select name="internetSpeed" data-placeholder="Select Internet Speed" class="select">
                                                        <option></option>
                                                        <option value="256 KB">256 KB</option> 
                                                        <option value="512 KB">512 KB</option> 
                                                        <option value="1 MB">1 MB</option> 
                                                        <option value="2 MB">2 MB</option> 
                                                        <option value="5 MB">5 MB</option> 
                                                    </select>
                                                </div>                                                                                                                       
                                            </div> 
                                        </div>
                                    </div>
                                </div>


                            </fieldset>
                        </div>-->
                    </div>

                    <div class="text-right">
                        <button type="submit" name="addLab" class="btn btn-primary">Add Lab <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /2 columns form -->


        <script>
            function acStatus(that) {
                if (that.value == "yes") {
                    document.getElementById("acStatus").style.display = "block";
                } else {
                    document.getElementById("acStatus").style.display = "none";            
                }
            }
        </script>

