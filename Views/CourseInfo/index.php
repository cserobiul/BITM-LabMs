<?php 
$courseInfo = $courseInfoObject->index();

?>
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">All Courses</li>
    </ul>
</div>
<br>
<div class="tab-pane">
    <a href="?p=addCourse">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Course</button>
    </a> 
</div>
<br>

<?php 
if(!empty($courseInfo)){ ?>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">All Course Information</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>

                </ul>
            </div>
        </div>

        <div class="panel-body"></div>

        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Course Name</th>
                    <th>Course Duration</th>
                    <th>Course Type</th>
                    <th>Course Start at</th>
                    <th colspan="3" class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $sl=1;
                foreach ($courseInfo as $item) { ?>

                    <tr>
                        <td><?php echo $sl++; ?></td>
                        <td><a href="?p=cDetails&id=<?php echo $item['unique_id'] ?>">
                            <?php echo ucwords($item['title']) ?></a> </td>
                            <td><?php echo ucwords($item['duration']) ?></td>
                            <td><?php echo ucwords($item['course_type']) ?></td>
                            <td><?php 
                                $originalDate = $item['created'];
                                $newDate = date("d-M-Y", strtotime($originalDate));
                                echo $newDate; ?>  </td>
                                <td class="text-center">


                                    <a href="?p=cDetails&id=<?php echo $item['unique_id'] ?>">
                                        <button type="button" class="btn bg-green-800 btn-labeled btn-rounded"><b><i class="icon-grid"></i></b>Details
                                        </button>
                                    </a>                            
                                    <a href="?p=editCourse&id=<?php echo $item['unique_id'] ?>">
                                        <button disabled type="button" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-pencil"></i></b>Edit
                                        </button>
                                    </a>                            
                                    <a href="?p=deleteCourse&id=<?php echo $item['unique_id'] ?>">
                                        <button disabled onclick="return delCheck();" type="button" class="btn bg-danger-400 btn-labeled btn-rounded"><b><i class="icon-trash"></i></b>Delete
                                        </button>
                                    </a>                        
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /basic datatable -->
                <script  type="text/javascript" >
                    function delCheck(){
                        return confirm('Are You Sure to Delete ??');
                    }
                </script>

                <?php }else{ ?>
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title">There are no  Lab </h5>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>

                                </ul>
                            </div>
                        </div>
                        <?php } ?>

