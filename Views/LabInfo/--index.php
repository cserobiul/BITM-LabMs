<?php 
$labInfoObject = new Apps\LabInfo\LabInfo();
$labInfo = $labInfoObject->index();

?>
<!-- extra menu link -->
    <div class="breadcrumb-line">
        <a href="?p=addLab">
            <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Lab</button>
        </a> 
    </div>
     <br>
<!-- extra menu link -->
<!-- Basic data table -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Lab Information</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                
            </ul>
        </div>
    </div>

    <div class="panel-body"> </div>

    <table class="table datatable-basic">
        <thead>
            <tr>
                <th>Course Name</th>
                <th>Lab No</th>
                <th>Seat Capacity</th>
                <th>Table Capacity</th>
                <th>Internet Speed</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            if(!empty($labInfo)){
                foreach ($labInfo as $item) { ?>

                    <tr>
                        <td><?php 
                            $_REQUEST['courseName']=$item['course_id'];
                            $getCourseName = $labInfoObject->assign($_REQUEST)->getCourseName();

                            echo $getCourseName['title'] ?>

                        </td>
                        <td><?php echo $item['lab_no'] ?></td>
                        <td><?php echo $item['seat_capacity'] ?></td>
                        <td><?php echo $item['table_capacity'] ?></td>
                        <td><?php echo $item['internet_speed'] ?></td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="?p=labShow&labNo=<?php echo $item['lab_no'] ?>"><i class="icon-file-pdf"></i> Details</a></li>
                                        <li><a href="#"><i class="icon-file-excel"></i> Edit</a></li>
                                        <li><a href="#"><i class="icon-file-word"></i> Delete</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>



                    <?php }

                }else{ ?>
                    <tr>
                        <td colspan="6">No Data</td>
                    </tr>
                <?php }

                ?>



            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->

