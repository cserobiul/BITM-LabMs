<?php
$data = $courseInfoObject->getCourseList();
?>
<script language="javascript" type="text/javascript">
    function getXMLHTTP() { 
        var xmlhttp=false;  
        try{
            xmlhttp=new XMLHttpRequest();
        }
        catch(e)    {       
            try{            
                xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch(e){
                try{
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch(e1){
                    xmlhttp=false;
                }
            }
        }

        return xmlhttp;
    }

    function getTrainer(course_name) {      

        var braURL="findTrainers.php?title="+course_name;
        var req = getXMLHTTP();
        
        if (req) {

            req.onreadystatechange = function() {
                if (req.readyState == 4) {

                    if (req.status == 200) {                        
                        document.getElementById('tranerdiv').innerHTML=req.responseText;

                    } else {
                        alert("Problem while using XMLHTTP:\n" + req.statusText);
                    }
                }               
            }           
            req.open("GET", braURL, true);
            req.send(null);
        }       
    }

    function getAsst(course_name) {     

        var braURL="findAsst.php?title="+course_name;
        var req = getXMLHTTP();
        
        if (req) {

            req.onreadystatechange = function() {
                if (req.readyState == 4) {

                    if (req.status == 200) {                        
                       document.getElementById('Asstdiv').innerHTML=req.responseText;
                   } else {
                    alert("Problem while using XMLHTTP:\n" + req.statusText);
                }
            }               
        }           
        req.open("GET", braURL, true);
        req.send(null);
    }       
}

function getAsstLab(course_name) {      

    var braURL="findLab.php?title="+course_name;
    var req = getXMLHTTP();

    if (req) {

        req.onreadystatechange = function() {
            if (req.readyState == 4) {

                if (req.status == 200) {                        

                   document.getElementById('Labdiv').innerHTML=req.responseText;
               } else {
                alert("Problem while using XMLHTTP:\n" + req.statusText);
            }
        }               
    }           
    req.open("GET", braURL, true);
    req.send(null);
}       
}

function getLabNo(lab_no) {     

    var braURL="findLabNo.php?title="+lab_no;
    var req = getXMLHTTP();

    if (req) {

        req.onreadystatechange = function() {
            if (req.readyState == 4) {

                if (req.status == 200) {                        
                    document.getElementById('LabNodiv').innerHTML=req.responseText;

                } else {
                    alert("Problem while using XMLHTTP:\n" + req.statusText);
                }
            }               
        }           
        req.open("GET", braURL, true);
        req.send(null);
    }       
}



</script>
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Add Lab</li>
    </ul>
</div>
<br>

<div class="tab-pane">
    <a href="?p=allLab">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> Show All Lab</button>
    </a> 
</div>
<br>
<!-- extra menu link -->
<form class="form-horizontal" method="POST" action="?p=labStore">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Add Lab Information</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <legend class="text-semibold"><i class="icon-reading position-left"></i> Add New Lab </legend>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Course Name:</label>
                            <div class="col-lg-9">
                            <select name="courseName" onChange="getTrainer(this.value); getAsst(this.value);getAsstLab(this.value);getLabNo(this.value)" data-placeholder="Select Course Name" class="select">
                                    <option></option>

                                    <?php
                                    if (!empty($data)) {
                                        foreach ($data as $value) {
                                            ?>
                                            <option value="<?php echo $value['unique_id'] ?>">
                                                <?php echo $value['title'] ?></option>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php if(isset($_SESSION['courseNameErrMsg']) && !empty($_SESSION['courseNameErrMsg'])){ ?>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-9">
                                        <?php echo $_SESSION['courseNameErrMsg'];
                                        unset($_SESSION['courseNameErrMsg']) ?>
                                    </div>
                                </div>
                                <?php } ?>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Lab No:</label>
                                    <div class="col-lg-9">
                                        <input name="labNo" type="number" class="form-control" placeholder="Lab No">
                                    </div>
                                </div>
                                <?php if(isset($_SESSION['labNoErrMsg']) && !empty($_SESSION['labNoErrMsg'])){ ?>
                                    <div class="form-group">
                                        <label class="col-lg-3 control-label"></label>
                                        <div class="col-lg-9">
                                            <?php echo $_SESSION['labNoErrMsg'];
                                            unset($_SESSION['labNoErrMsg']) ?>
                                        </div>
                                    </div>
                                    <?php } ?>


                                    <div class="form-group">
                                        <label class="col-lg-3 control-label">Seat Capacity:</label>
                                        <div class="col-lg-9">
                                            <input name="seatCapacity" type="number" class="form-control" placeholder="Seat Capacity">
                                        </div>
                                    </div>

                                    <?php if(isset($_SESSION['seatCapacityErrMsg']) && !empty($_SESSION['seatCapacityErrMsg'])){ ?>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-lg-9">
                                                <?php echo $_SESSION['seatCapacityErrMsg'];
                                                unset($_SESSION['seatCapacityErrMsg']) ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Table Capacity:</label>
                                            <div class="col-lg-9">
                                                <input name="tableCapacity" type="number" class="form-control" placeholder="Table Capacity">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Projector Resolution:</label>
                                            <div class="col-lg-9">
                                                <input name="proResolution" type="text" class="form-control" placeholder="Ex:  1024x768">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">AC Status:</label>
                                            <div class="col-lg-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select onchange="acStatus(this);" name="acStauts" data-placeholder="Select AC Status" class="select">
                                                                <option></option>
                                                                <option value="yes">Yes</option> 
                                                                <option value="no">No</option> 
                                                            </select>
                                                        </div>                                                
                                                    </div> 

                                                    <div class="col-md-6" id="acStatus" style="display: none;">
                                                        <input name="acStauts" value="0" type="text" placeholder="How many AC" class="form-control mb-15">

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold"> &nbsp;</legend>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Student PC Configuration:</label>
                                            <div class="col-lg-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="processor" data-placeholder="Select Processor" class="select">
                                                                <option></option>
                                                                <option value="celeron">Celeron</option> 
                                                                <option value="core 2 due">Core 2 Due</option> 
                                                                <option value="core 2 quad">Core 2 Quad</option> 
                                                                <option value="core i5">Core i5</option> 
                                                                <option value="core i7">Core i7</option> 
                                                            </select>
                                                        </div>                                                                                                                       
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="ram" data-placeholder="Select RAM" class="select">
                                                                <option></option>
                                                                <option value="1 gb">1 GB</option> 
                                                                <option value="2 gb">2 GB</option> 
                                                                <option value="4 gb">4 GB</option> 
                                                                <option value="8 gb">8 GB</option> 
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="brand" data-placeholder="Select Brand" class="select">
                                                                <option></option>
                                                                <option value="dell">Dell</option> 
                                                                <option value="sumsang">Sumsang</option> 
                                                                <option value="lg">LG</option> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="os" data-placeholder="Select OS" class="select">
                                                                <option></option>
                                                                <option value="windows 7">Windows 7</option> 
                                                                <option value="windows 8">Windows 8</option> 
                                                                <option value="windows 8.1">Windows 8.1</option> 
                                                                <option value="windows 10">Windows 10</option> 
                                                                <option value="redhat linux">RedHat Linux</option> 
                                                                <option value="cent os">Cent OS Linux</option> 
                                                                <option value="mac">Mac</option> 
                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>

                                        <!-- trainer pc config -->
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Trainer PC Configuration:</label>
                                            <div class="col-lg-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="tprocessor" data-placeholder="Select Processor" class="select">
                                                                <option></option>
                                                                <option value="celeron">Celeron</option> 
                                                                <option value="core 2 due">Core 2 Due</option> 
                                                                <option value="core 2 quad">Core 2 Quad</option> 
                                                                <option value="core i5">Core i5</option> 
                                                                <option value="core i7">Core i7</option> 
                                                            </select>
                                                        </div>                                                                                                                       
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="tram" data-placeholder="Select RAM" class="select">
                                                                <option></option>
                                                                <option value="1 gb">1 GB</option> 
                                                                <option value="2 gb">2 GB</option> 
                                                                <option value="4 gb">4 GB</option> 
                                                                <option value="8 gb">8 GB</option> 
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="tbrand" data-placeholder="Select Brand" class="select">
                                                                <option></option>
                                                                <option value="dell">Dell</option> 
                                                                <option value="sumsang">Sumsang</option> 
                                                                <option value="lg">LG</option> 
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="tos" data-placeholder="Select OS" class="select">
                                                                <option></option>
                                                                <option value="windows 7">Windows 7</option> 
                                                                <option value="windows 8">Windows 8</option> 
                                                                <option value="windows 8.1">Windows 8.1</option> 
                                                                <option value="windows 10">Windows 10</option> 
                                                                <option value="redhat linux">RedHat Linux</option> 
                                                                <option value="cent os">Cent OS Linux</option> 
                                                                <option value="mac">Mac</option> 
                                                            </select>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Internet Speed:</label>
                                            <div class="col-lg-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="mb-15">
                                                            <select name="internetSpeed" data-placeholder="Select Internet Speed" class="select">
                                                                <option></option>
                                                                <option value="256 KB">256 KB</option> 
                                                                <option value="512 KB">512 KB</option> 
                                                                <option value="1 MB">1 MB</option> 
                                                                <option value="2 MB">2 MB</option> 
                                                                <option value="5 MB">5 MB</option> 
                                                            </select>
                                                        </div>                                                                                                                       
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>


                                    </fieldset>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" name="addLab" class="btn btn-primary">Add Lab <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- /2 columns form -->


                <script>
                    function acStatus(that) {
                        if (that.value == "yes") {
                            document.getElementById("acStatus").style.display = "block";
                        } else {
                            document.getElementById("acStatus").style.display = "none";            
                        }
                    }
                </script>

