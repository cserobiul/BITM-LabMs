<?php 
$labInfo = $labInfoObject->index();

?>
<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">All Lab</li>
    </ul>
</div>
<br>
<div class="tab-pane">
    <a href="?p=addLab">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Lab</button>
    </a> 
</div>
<br>

<?php 
if(!empty($labInfo)){ ?>
    <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">All Lab Information</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>

                    </ul>
                </div>
            </div>

            <div class="panel-body"></div>

            <table class="table datatable-tools-basic">
                <thead>
                    <tr>
                        <th>Lab No</th>
                        <th>Seat Capacity</th>
                        <th>Table Capacity</th>
                        <th>Internet Speed</th>
                        <th>AC Status</th>
                        <th colspan="3" class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($labInfo as $item) { ?>

                            <tr>
                                <td><a href="?p=labShow&labNo=<?php echo $item['lab_no'] ?>">
                                    <?php echo $item['lab_no'] ?></a> </td>
                                    <td><?php echo $item['seat_capacity'] ?></td>
                                    <td><?php echo $item['table_capacity'] ?></td>
                                    <td><?php echo $item['internet_speed'] ?></td>
                                    <td><?php echo $item['ac_status'] ?></td>
                                    <td class="text-center">


                                        <a href="?p=labShow&labNo=<?php echo $item['lab_no'] ?>">
                                            <button type="button" class="btn bg-green-800 btn-labeled btn-rounded"><b><i class="icon-grid"></i></b>Details
                                            </button>
                                        </a>                            
                                        <a href="?p=editLab&labNo=<?php echo $item['lab_no'] ?>">
                                            <button type="button" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-pencil"></i></b>Edit
                                            </button>
                                        </a>                            
                                        <a href="?p=deleteLab&labNo=<?php echo $item['lab_no'] ?>">
                                            <button onclick="return delCheck();" type="button" class="btn bg-danger-400 btn-labeled btn-rounded"><b><i class="icon-trash"></i></b>Delete
                                            </button>
                                        </a>                        
                                    </td>
                                </tr>
                                <?php } ?>
                           </tbody>
                        </table>
                    </div>
                    <!-- /basic datatable -->
                    <script  type="text/javascript" >
                        function delCheck(){
                            return confirm('Are You Sure to Delete ??');
                        }
                    </script>

<?php }else{ ?>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">There are no  Lab </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>

                </ul>
            </div>
        </div>
        <?php } ?>

