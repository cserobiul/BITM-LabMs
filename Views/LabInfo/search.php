
<!-- Search field -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Website search results</h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
				<li><a data-action="close"></a></li>
			</ul>
		</div>
	</div>

	<div class="panel-body">
		<form action="#" class="main-search">
			<div class="input-group content-group">
				<div class="has-feedback has-feedback-left">
					<input type="text" class="form-control input-xlg" value="Limitless interface kit">
					<div class="form-control-feedback">
						<i class="icon-search4 text-muted text-size-base"></i>
					</div>
				</div>

				<div class="input-group-btn">
					<button type="submit" class="btn btn-primary btn-xlg">Search</button>
				</div>
			</div>

			<div class="row search-option-buttons">
				<div class="col-sm-6">
					<ul class="list-inline list-inline-condensed no-margin-bottom">
						<li class="dropdown">
							<a href="#" class="btn btn-link btn-sm dropdown-toggle" data-toggle="dropdown">
								<i class="icon-stack2 position-left"></i> All categories <span class="caret"></span>
							</a>

							<ul class="dropdown-menu">
								<li><a href="#"><i class="icon-question7"></i> Getting started</a></li>
								<li><a href="#"><i class="icon-accessibility"></i> Registration</a></li>
								<li><a href="#"><i class="icon-reading"></i> General info</a></li>
								<li><a href="#"><i class="icon-gear"></i> Your settings</a></li>
								<li><a href="#"><i class="icon-graduation"></i> Copyrights</a></li>
								<li class="divider"></li>
								<li><a href="#"><i class="icon-mail-read"></i> Contacting authors</a></li>
							</ul>
						</li>
						<li><a href="#" class="btn btn-link btn-sm"><i class="icon-reload-alt position-left"></i> Refine your search</a></li>
					</ul>
				</div>

				<div class="col-sm-6 text-right">
					<ul class="list-inline no-margin-bottom">
						<li><a href="#" class="btn btn-link btn-sm"><i class="icon-make-group position-left"></i> Browse website</a></li>
						<li><a href="#" class="btn btn-link btn-sm"><i class="icon-menu7 position-left"></i> Advanced search</a></li>
					</ul>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- /search field -->


<!-- Tabs -->
<ul class="nav nav-lg nav-tabs nav-tabs-bottom search-results-tabs">
	<li><a href="search_basic.html"><i class="icon-display4 position-left"></i> Website</a></li>
	<li class="active"><a href="search_users.html"><i class="icon-people position-left"></i> Users</a></li>
	<li><a href="search_images.html"><i class="icon-image2 position-left"></i> Images</a></li>
	<li><a href="search_videos.html"><i class="icon-file-play position-left"></i> Videos</a></li>
	<li class="dropdown pull-right">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="visible-xs-inline-block position-right">Options</span> <span class="caret"></span></a>
		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="#">Action</a></li>
			<li><a href="#">Another action</a></li>
			<li><a href="#">Something else</a></li>
			<li class="divider"></li>
			<li><a href="#">One more line</a></li>
		</ul>
	</li>
</ul>
<!-- /tabs -->


<!-- Search results -->
<div class="content-group">
	<p class="text-muted text-size-small content-group">About 827,000 results (0.34 seconds)</p>

	<div class="search-results-list content-group">
		<div class="row">
			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">James Alexander <small class="display-block">Lead developer</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Nathan Jacobson <small class="display-block">Lead UX designer</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Margo Baker <small class="display-block">Sales manager</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Barbara Walden <small class="display-block">SEO specialist</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Hanna Dorman <small class="display-block">UX/UI designer</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Benjamin Loretti <small class="display-block">Network engineer</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Vanessa Aurelius <small class="display-block">Front end guru</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">William Brenson <small class="display-block">Chief officer</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Lucy North <small class="display-block">PHP developer</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Vicky Barna <small class="display-block">UI designer</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Hugo Bills <small class="display-block">Sales manager</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-sm-6">
				<div class="thumbnail">
					<div class="thumb thumb-rounded">
						<img src="assets/images/placeholder.jpg" alt="">
						<div class="caption-overflow">
							<span>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded"><i class="icon-plus2"></i></a>
								<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
							</span>
						</div>
					</div>
					
					<div class="caption text-center">
						<h6 class="text-semibold no-margin">Tony Gurrano <small class="display-block">CEO and founder</small></h6>
						<ul class="icons-list mt-15">
							<li><a href="#" data-popup="tooltip" title="Google Drive"><i class="icon-google-drive"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Twitter"><i class="icon-twitter"></i></a></li>
							<li><a href="#" data-popup="tooltip" title="Github"><i class="icon-github"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>


		<div class="text-size-small text-uppercase text-semibold text-muted mb-10">Cards view</div>
		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">James Alexander</h6>
							<span class="text-muted">Lead developer</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Nathan Jacobson</h6>
							<span class="text-muted">Lead UX designer</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Margo Baker</h6>
							<span class="text-muted">Sales manager</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Barbara Walden</h6>
							<span class="text-muted">SEO specialist</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Hanna Dorman</h6>
							<span class="text-muted">UX/UI designer</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Benjamin Loretti</h6>
							<span class="text-muted">Network engineer</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Vanessa Aurelius</h6>
							<span class="text-muted">Front end guru</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">William Brenson</h6>
							<span class="text-muted">Chief officer</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Lucy North</h6>
							<span class="text-muted">PHP developer</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Vicky Barna</h6>
							<span class="text-muted">UI designer</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Hugo Bills</h6>
							<span class="text-muted">Sales manager</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="panel panel-body">
					<div class="media">
						<a href="#" class="media-left">
							<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
						</a>

						<div class="media-body">
							<h6 class="media-heading">Tony Gurrano</h6>
							<span class="text-muted">CEO and founder</span>
						</div>

						<div class="media-right media-middle">
							<ul class="icons-list icons-list-vertical">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
										<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
										<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
										<li class="divider"></li>
										<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="text-size-small text-uppercase text-semibold text-muted mb-10">List view</div>
		<div class="panel panel-body">
			<ul class="media-list">
				<li class="media-header text-muted">Team leaders</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">James Alexander</div>
						<span class="text-muted">Development</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Jeremy Victorino</div>
						<span class="text-muted">Finances</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Margo Baker</div>
						<span class="text-muted">Marketing</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Monica Smith</div>
						<span class="text-muted">Design</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media-header text-muted">Office staff</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Bastian Miller</div>
						<span class="text-muted">Web dev</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Jordana Mills</div>
						<span class="text-muted">Sales consultant</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Buzz Brenson</div>
						<span class="text-muted">UX expert</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Zachary Willson</div>
						<span class="text-muted">Illustrator</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">William Miles</div>
						<span class="text-muted">SEO expert</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media-header text-muted">Partners</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Freddy Walden</div>
						<span class="text-muted">Microsoft</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Dori Laperriere</div>
						<span class="text-muted">Google</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>

				<li class="media">
					<div class="media-left media-middle">
						<a href="#">
							<img src="assets/images/placeholder.jpg" class="img-circle" alt="">
						</a>
					</div>

					<div class="media-body">
						<div class="media-heading text-semibold">Vanessa Aurelius</div>
						<span class="text-muted">Facebook</span>
					</div>

					<div class="media-right media-middle">
						<ul class="icons-list text-nowrap">
							<li class="dropup">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu9"></i></a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-comment-discussion pull-right"></i> Start chat</a></li>
									<li><a href="#"><i class="icon-phone2 pull-right"></i> Make a call</a></li>
									<li><a href="#"><i class="icon-mail5 pull-right"></i> Send mail</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-statistics pull-right"></i> Statistics</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</div>

	</div>

	<ul class="pagination pagination-flat text-center pagination-xs no-margin-bottom">
		<li class="disabled"><a href="#">&larr;</a></li>
		<li class="active"><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">4</a></li>
		<li><span>...</span></li>
		<li><a href="#">58</a></li>
		<li><a href="#">59</a></li>
		<li><a href="#">&rarr;</a></li>
	</ul>
</div>
<!-- /search results -->


