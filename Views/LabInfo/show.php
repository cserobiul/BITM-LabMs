
<?php
$data = $labInfoObject->assign($_REQUEST)->show();
?>

<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li><a href="?p=allLab">All Lab</a></li>
        <li class="active">Lab Details</li>
    </ul>
</div>
<br>
    <div class="tab-pane">
        <a href="?p=addLab">
            <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Lab</button>
        </a> 
        <a href="?p=allLab">
            <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> Show All Lab</button>
        </a> 
    </div>
    <br>
    <?php if (!empty($data)) { ?>
        <div class="panel panel-white">
            <div class="panel-heading">
                <h5 class="panel-title">Lab Details Information </h5>
                <div class="heading-elements">
                    <a href="?p=editLab&labNo=<?php echo $data['lab_no'] ?>">
                        <button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-pencil position-left"></i> Edit</button>
                    </a>
                    <button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
                </div>
            </div>



            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                        <tr>
                            <th class="col-sm-1">Item</th>
                            <th class="col-sm-1">Description</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>
                                <h6 class="no-margin">Lab No</h6>                                
                            </td>
                            <td><?php echo $data['lab_no'] ?></td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="no-margin">Lab For </h6>                                
                            </td>
                            <td><?php 
                            $_REQUEST['courseName'] = $data['course_id'];
                            $courseName = $courseInfoObject->assign($_REQUEST)->getCourseName();
                            echo ucwords($courseName['title']);  ?></td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="no-margin">Seat Capacity</h6>                                
                            </td>
                            <td><?php echo $data['seat_capacity'] ?> </td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="no-margin">Projector Resolution</h6>                                
                            </td>
                            <td><?php echo $data['projector_resolution'] ?> </td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="no-margin">AC Status</h6>                                
                            </td>
                            <td><?php echo $data['ac_status'] ?>  </td>
                        </tr>
                        <tr>
                            <td>
                                <h6 class="no-margin">Student PC Configuration</h6>                                
                            </td>
                            <td><?php 
                                $pcc = unserialize($data['pc_configuration']);

                                echo 'Processor: '.  strtoupper($pcc['processor']);
                                echo '<br>';
                                echo 'Ram: '.  strtoupper($pcc['ram']);
                                echo '<br>';
                                echo 'Brand: '.  strtoupper($pcc['brand']);
                                echo '<br>';
                                echo 'OS: '.  ucwords($pcc['os']);

                                ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <h6 class="no-margin">Trainer PC Configuration</h6>                                
                                </td>
                                <td><?php 
                                    $tpcc = unserialize($data['trainer_pc_configuration']);

                                    echo 'Processor: '.  strtoupper($tpcc['processor']);
                                    echo '<br>';
                                    echo 'Ram: '.  strtoupper($tpcc['ram']);
                                    echo '<br>';
                                    echo 'Brand: '.  strtoupper($tpcc['brand']);
                                    echo '<br>';
                                    echo 'OS: '.  ucwords($tpcc['os']);
                                    
                                    ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="no-margin">Table Capacity</h6>                                
                                    </td>
                                    <td><?php echo $data['table_capacity'] ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="no-margin">Internet Speed</h6>                                
                                    </td>
                                    <td><?php echo $data['internet_speed'] ?></td>
                                </tr>


                            </tbody>
                        </table>
                    </div>


                </div>
                <?php
            } else { ?>
             <div class="panel panel-white">
                <div class="panel-heading">
                <h5 class="panel-title">There are no Lab Details Information that you want</h5>
                    
                </div>
            </div>
            <?php }
            ?>
            <!-- /invoice template -->



