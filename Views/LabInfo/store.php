<?php
if (isset($_REQUEST['addLab'])) {
    $chkLabno = $labInfoObject->assign($_REQUEST)->chkLabNo();
    $errors = array();

    // course name 
    if (empty($_REQUEST['courseName'])) {
        $errors[] = 'Course Name Required...';
        $_SESSION['courseNameErrMsg'] = '<font color="red">Course Name Required...</font>';
    }

    // labno

    if (empty($_REQUEST['labNo'])) {
        $errors[] = 'Lab No Required...';
        $_SESSION['labNoErrMsg'] = '<font color="red">Lab No Required...</font>';
    } elseif ($chkLabno > 0) {
        $errors[] = 'Lab No Already Exist...';
        $_SESSION['labNoErrMsg'] = '<font color="red">Lab No Already Exist</font>';
        unset($_REQUEST['labNo']);
    } else {
        $_SESSION['labNoValue'] = $_REQUEST['labNo'];
    }

    // seatCapacity

    if (empty($_REQUEST['seatCapacity'])) {
        $errors[] = 'Seat Capacity Required...';
        $_SESSION['seatCapacityErrMsg'] = '<font color="red">Seat Capacity Required...</font>';
    } else {
        $_SESSION['seatCapacityValue'] = $_REQUEST['seatCapacity'];
    }


    if (empty($errors) == true) {
        $data = $labInfoObject->assign($_REQUEST)->store();

        if ($data == 1) {
            $_SESSION['msg'] = "Added a Lab info Successfully";
            unset($_SESSION['labNoValue']);
            unset($_SESSION['seatCapacityValue']);
            echo '<script type="text/javascript">location.replace("?p=allLab");</script>';
        } else {
            echo $_SESSION['msg'] = "Error While Lab info Submitted";
           // echo '<script type="text/javascript">location.replace("?p=addLab");</script>';
        }
    } else {
        echo '<script type="text/javascript">location.replace("?p=addLab");</script>';
        foreach ($errors as $key) {
            echo '<p style="color:red;">' . $key . '</p>';
        }
    }
} else {
    
   echo '<script type="text/javascript">location.replace("?p=404");</script>';
}
?>