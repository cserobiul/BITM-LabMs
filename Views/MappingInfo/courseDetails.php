<script src="" type="text/javascript">
	function printDiv() {
		var DocumentContainer = document.getElementById('invc');
		var WindowObject = window.open('', 'PrintWindow', 'width=1000,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');
		var strHtml = "<html>\n<head>\n <link rel=\"stylesheet\" type=\"text/css\" href=\"test.css\">\n</head><body><div style=\"testStyle\">\n" + DocumentContainer.innerHTML + "\n</div>\n</body>\n</html>";
		WindowObject.document.writeln(strHtml);
		WindowObject.document.close();
		WindowObject.focus();
		WindowObject.print();
		WindowObject.close();

	}
</script>
<?php
$data = $MappingInfoObject->assign($_REQUEST)->show();

?>

<div class="breadcrumb-line">
	<ul class="breadcrumb">
		<li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
		<li><a href="?p=assigned">All Assigned Courses</a></li>
		<li class="active">Assigned Course Details</li>
	</ul>
</div>
<br>

<div class="tab-pane">
	<a href="?p=assignCourse">
		<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Assign New Course</button>
	</a> 
	<a href="?p=assigned">
		<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> Show All Assigned Courses</button>
	</a> 
</div>
<br>
<!-- extra menu link -->

<!-- Invoice template -->
<?php if(!empty($data)){ 
	$_REQUEST['id'] = $data['lead_trainer'];
	$leadTrainerName = $TrainerInfoObject->assign($_REQUEST)->leadTrainerName();
	$_REQUEST['id'] = $data['asst_trainer'];
	$assistantTrainerName = $TrainerInfoObject->assign($_REQUEST)->assistantTrainerName();
	$_REQUEST['id'] = $data['lab_asst'];
	$labAssistantName = $TrainerInfoObject->assign($_REQUEST)->labAssistantName();

	?>
	<div class="panel panel-white" id="invc">
		<div class="panel-heading">
			<h4 class="panel-title">Assigned Courses Details Information </h4>
			<div class="heading-elements">
				<a href="?p=editassignedCourse&id=<?php echo $data['course_id'] ?>">
					<button type="button" class="btn btn-default btn-xs heading-btn"><i class="icon-pencil position-left"></i> Edit</button>
				</a>
				<button type="button" onclick="printDiv();" class="btn btn-default btn-xs heading-btn"><i class="icon-printer position-left"></i> Print</button>
			</div>
		</div>



		<div class="table-responsive">
			<table class="table table-lg">

				<tbody>

					<tr>
						<td>
							<h6 class="no-margin">Course Name</h6>                                
						</td>
						<td><?php 
							$_REQUEST['courseName']=$data['course_id'];
							$getCourseName = $courseInfoObject->assign($_REQUEST)->getCourseName();
							echo ucwords($getCourseName['title']); ?>

						</td>
					</tr>
					<tr>
						<td>
							<h6 class="no-margin">Batch No</h6>                                
						</td>
						<td><?php echo $data['batch_no'] ?> </td>
					</tr>
					<tr>
						<td>
							<h6 class="no-margin">Lead Trainer</h6>                                
						</td>
						<td><a href="?p=trainerDetail&id=<?php echo $data['lead_trainer'] ?>" >
							<?php echo ucwords($leadTrainerName['full_name']) ?></a> </td>
						</tr>
						<tr>
							<td>
								<h6 class="no-margin">Assistant Trainer</h6>                                
							</td>
							<td><a href="?p=trainerDetail&id=<?php echo $data['asst_trainer'] ?>" >
								<?php echo ucwords($assistantTrainerName['full_name']) ?></a> </td>
							</tr>
							<tr>
								<td>
									<h6 class="no-margin">Lab Assistant</h6>                                
								</td>
								<td><a href="?p=trainerDetail&id=<?php echo $data['lab_asst'] ?>" >
									<?php echo ucwords($labAssistantName['full_name']) ?> </a></td>
								</tr>
								<tr>
									<td>
										<h6 class="no-margin">Lab No</h6>                                
									</td>
									<td><?php echo $data['lab_id'] ?> </td>
								</tr>
								<tr>
									<td>
										<h6 class="no-margin">Start Date</h6>                                
									</td>
									<td><?php 
										$start_date = $data['start_date'];
										$start_date = date("d-M-Y", strtotime($start_date));
										echo $start_date;
										?>  </td>
									</tr>


									<tr>
										<td>
											<h6 class="no-margin">End Date</h6>                                
										</td>
										<td><?php 
											$ending_date = $data['ending_date'];
											$ending_date = date("d-M-Y", strtotime($ending_date));
											echo $ending_date;
											?>  </td>
										</tr>
										<tr>
											<td>
												<h6 class="no-margin">Weekly Day</h6>                                
											</td>
											<td><?php if($data['day'] == 1){
												echo 'Friday';
											}elseif ($data['day'] == 2) {
												echo 'Sat-Mon-Wed';
											}elseif ($data['day'] = 3) {
												echo 'Sun-Tue-Thu';
											}else{
												echo 'NA';
											} ?></td>
										</tr>
										<tr>
											<td>
												<h6 class="no-margin">Class Time</h6>                                
											</td>
											<td><?php 
												if($data['start_time']<= '12.00'){
													echo $data['start_time'].' AM';
												}else{
													$stime = $data['start_time']-'12.00';  

													echo number_format($stime, 2, '.', '').' PM';  
												}
												echo ' to ';
												if($data['ending_time'] <='12.00'){
													echo $data['ending_time'].' AM';
												}else{
													$etime = $data['ending_time']-'12.00';
													echo number_format($etime,2,'.','').' PM';
												}      

												?></td>
											</tr>


										</tbody>
									</table>
								</div>


							</div>

							<?php }else{ 
								echo '<script type="text/javascript">location.replace("?p=404");</script>';
							} ?>



