<?php
$data = $MappingInfoObject->assign($_REQUEST)->show();

$courseList = $courseInfoObject->getCourseList();
$labList = $MappingInfoObject->getLabList();

$leadTrainerList = $TrainerInfoObject->leadTrainer();
$assistantTrainerList = $TrainerInfoObject->assistantTrainer();
$labsupporter = $TrainerInfoObject->labSupporter();
?>

<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li><a href="?p=assigned">All Assigned Courses</a></li>
        <li class="active">Edit Assaigned Course</li>
    </ul>
</div>
<br>
<div class="tab-pane">
    <a href="?p=assignCourse">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Assign New Course</button>
    </a> 
    <a href="?p=assigned">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-grid position-left"></i> All Assaign Courses </button>
    </a> 
</div>
<br>
<!-- extra menu link -->
<?php 
if(!empty($data)){
    $_REQUEST['id'] = $data['lead_trainer'];
    $leadTrainerName = $TrainerInfoObject->assign($_REQUEST)->leadTrainerName();
    $_REQUEST['id'] = $data['asst_trainer'];
    $assistantTrainerName = $TrainerInfoObject->assign($_REQUEST)->assistantTrainerName();
    $_REQUEST['id'] = $data['lab_asst'];
    $labAssistantName = $TrainerInfoObject->assign($_REQUEST)->labAssistantName();
    ?>
    <form class="form-horizontal" method="POST" action="?p=updateAssaignCourses">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Edit Assaign Course</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Edit Assaign Course </legend>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Course Name:</label>
                                <div class="col-lg-9">
                                    <?php 
                                    $_REQUEST['courseName']=$data['course_id'];
                                    $getCourseName = $courseInfoObject->assign($_REQUEST)->getCourseName();

                                    echo '<h3 class="no-margin">'.ucwords($getCourseName['title']).'</h3>';
                                    $_SESSION['cn'] = $data['course_id'];
                                    ?>
                                    
                                </div>

                            </div>
                            <?php if(isset($_SESSION['courseNameErrMsg']) && !empty($_SESSION['courseNameErrMsg'])){ ?>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label"></label>
                                    <div class="col-lg-9">
                                        <?php echo $_SESSION['courseNameErrMsg'];
                                        unset($_SESSION['courseNameErrMsg']) ?>
                                    </div>
                                </div>
                                <?php } ?>


                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Lab No:</label>
                                    <div class="col-lg-9">
                                        <select  name="labNo" data-placeholder="Select Lab" class="select">
                                            <option></option>
                                            <option selected> <?php echo $data['lab_id'] ?></option>

                                            <?php
                                            if (!empty($labList)) {
                                                foreach ($labList as $value) {
                                                    ?>
                                                    <option value="<?php echo $value['lab_no'] ?>">
                                                        <?php echo $value['lab_no'] ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if(isset($_SESSION['labNoErrMsg']) && !empty($_SESSION['labNoErrMsg'])){ ?>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-lg-9">
                                                <?php echo $_SESSION['labNoErrMsg'];
                                                unset($_SESSION['labNoErrMsg']) ?>
                                            </div>
                                        </div>
                                        <?php } ?>


                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Lead Trainer:</label>
                                            <div class="col-lg-9">
                                             <select name="lead_trainer" data-placeholder="Select Lead Trainer" class="select">
                                              <option></option>
                                              <option selected> 
                                                  <?php echo ucwords($leadTrainerName['full_name']) ?>

                                              </option>

                                              <?php
                                              if (!empty($leadTrainerList)) {
                                                foreach ($leadTrainerList as $value) {
                                                    ?>
                                                    <option value="<?php echo $value['unique_id'] ?>">
                                                        <?php echo $value['full_name'] ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php if(isset($_SESSION['lead_trainerErrMsg']) && !empty($_SESSION['lead_trainerErrMsg'])){ ?>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-lg-9">
                                                <?php echo $_SESSION['lead_trainerErrMsg'];
                                                unset($_SESSION['lead_trainerErrMsg']) ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Assistant Trainer:</label>
                                            <div class="col-lg-9">
                                             <select name="asst_trainer" data-placeholder="Select Assistant Trainer" class="select">
                                              <option></option>
                                              <option selected> 
                                                  <?php echo ucwords($assistantTrainerName['full_name']) ?>

                                              </option>

                                              <?php
                                              if (!empty($assistantTrainerList)) {
                                                foreach ($assistantTrainerList as $value) {
                                                    ?>
                                                    <option value="<?php echo $value['unique_id'] ?>">
                                                        <?php echo ucwords($value['full_name']) ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if(isset($_SESSION['asst_trainerErrMsg']) && !empty($_SESSION['asst_trainerErrMsg'])){ ?>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-lg-9">
                                                <?php echo $_SESSION['asst_trainerErrMsg'];
                                                unset($_SESSION['asst_trainerErrMsg']) ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Lab Assistant:</label>
                                            <div class="col-lg-9">
                                             <select name="lab_asst" data-placeholder="Select Lab Supporter" class="select">
                                              <option></option>
                                              <option selected> 
                                                  <?php echo ucwords($labAssistantName['full_name']) ?>

                                              </option>


                                              <?php
                                              if (!empty($labsupporter)) {
                                                foreach ($labsupporter as $value) {
                                                    ?>
                                                    <option value="<?php echo $value['unique_id'] ?>">
                                                        <?php echo ucwords($value['full_name']) ?></option>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php if(isset($_SESSION['lab_asstErrMsg']) && !empty($_SESSION['lab_asstErrMsg'])){ ?>
                                        <div class="form-group">
                                            <label class="col-lg-3 control-label"></label>
                                            <div class="col-lg-9">
                                                <?php echo $_SESSION['lab_asstErrMsg'];
                                                unset($_SESSION['lab_asstErrMsg']) ?>
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </fieldset>
                                </div>

                                <div class="col-md-6">
                                    <fieldset>
                                        <legend class="text-semibold"> &nbsp;</legend>

                                        <div class="form-group">
                                            <label class="col-lg-3 control-label">Batch No:</label>
                                            <div class="col-lg-9">
                                                <input name="batch_no" type="number" class="form-control" placeholder="Batch No"
                                                value="<?php echo $data['batch_no'] ?>">
                                            </div>
                                        </div>
                                        <?php if(isset($_SESSION['batch_noErrMsg']) && !empty($_SESSION['batch_noErrMsg'])){ ?>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label"></label>
                                                <div class="col-lg-9">
                                                    <?php echo $_SESSION['batch_noErrMsg'];
                                                    unset($_SESSION['batch_noErrMsg']) ?>
                                                </div>
                                            </div>
                                            <?php } ?>

                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Course Date:</label>
                                                <div class="col-lg-9">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                                <?php $sdate = $data['start_date']; ?>
                                                                <input name="start_date" type="text"  class="form-control pickadate-selectors" placeholder="Start Date" value='<?php echo $sdate; ?>'>
                                                            </div>  

                                                            <?php if(isset($_SESSION['start_dateErrMsg']) && !empty($_SESSION['start_dateErrMsg'])){ ?>

                                                                <div class="input-group">
                                                                    <?php echo $_SESSION['start_dateErrMsg'];
                                                                    unset($_SESSION['start_dateErrMsg']) ?>
                                                                </div>

                                                                <?php } ?>                                                                                                                    
                                                            </div>






                                                            <div class="col-md-6">

                                                                <div class="input-group">
                                                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                                    <input name="ending_date" type="text"   class="form-control pickadate-selectors" placeholder="End Date" value='<?php echo $data['ending_date'] ?>'>

                                                                </div>

                                                                <?php if(isset($_SESSION['ending_dateErrMsg']) && !empty($_SESSION['ending_dateErrMsg'])){ ?>

                                                                    <div class="input-group">
                                                                        <?php echo $_SESSION['ending_dateErrMsg'];
                                                                        unset($_SESSION['ending_dateErrMsg']) ?>
                                                                    </div>

                                                                    <?php } ?>
                                                                </div>





                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- trainer pc config -->
                                                    <div class="form-group">
                                                        <label class="col-lg-3 control-label">Course Day:</label>
                                                        <div class="col-lg-9">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="mb-15">
                                                                        <select name="day" data-placeholder="Select Day" class="select">
                                                                            <option></option>
                                                                            <option value="1">Friday</option> 
                                                                            <option value="2">Sat-Mon-Wed</option> 
                                                                            <option value="3">Sun-Tue-Thu</option>
                                                                        </select>
                                                                    </div>  

                                                                    <?php if(isset($_SESSION['dayErrMsg']) && !empty($_SESSION['dayErrMsg'])){ ?>

                                                                        <div class="mb-15">
                                                                            <?php echo $_SESSION['dayErrMsg'];
                                                                            unset($_SESSION['dayErrMsg']) ?>
                                                                        </div>

                                                                        <?php } ?>                                                                                                                      
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">Course Time:</label>
                                                            <div class="col-lg-9">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="mb-15">
                                                                            <select name="start_time" data-placeholder="Start Time" class="select">
                                                                                <option></option>
                                                                                <option selected value='<?php echo $data['start_time'] ?>'>

                                                                                    <?php if($data['start_time']<= '12.00'){
                                                                                        echo $data['start_time'].' AM';
                                                                                    }else{
                                                                                        $stime = $data['start_time']-'12.00';  

                                                                                        echo number_format($stime, 2, '.', '').' PM';  
                                                                                    } ?></option>

                                                                                    <option value="8.00">8.00 AM</option> 
                                                                                    <option value="8.30">8.30 AM</option> 
                                                                                    <option value="9.00">9.00 AM</option> 
                                                                                    <option value="9.30">9.30 AM</option> 
                                                                                    <option value="10.00">10.00 AM</option> 
                                                                                    <option value="10.30">10.30 AM</option> 
                                                                                    <option value="11.00">11.00 AM</option> 
                                                                                    <option value="11.30">11.30 AM</option> 
                                                                                    <option value="12.00">12.00 PM</option> 
                                                                                    <option value="12.30">12.30 PM</option> 
                                                                                    <option value="13.00">1.00 PM</option> 
                                                                                    <option value="13.30">1.30 PM</option> 
                                                                                    <option value="14.00">2.00 PM</option> 
                                                                                    <option value="14.30">2.30 PM</option> 
                                                                                    <option value="15.00">3.00 PM</option> 
                                                                                    <option value="15.30">3.30 PM</option> 
                                                                                    <option value="16.00">4.00 PM</option> 
                                                                                    <option value="16.30">4.30 PM</option> 
                                                                                    <option value="17.00">5.00 PM</option> 
                                                                                    <option value="17.30">5.30 PM</option> 
                                                                                    <option value="18.00">6.00 PM</option> 
                                                                                    <option value="18.30">6.30 PM</option> 
                                                                                    <option value="19.00">7.00 PM</option> 
                                                                                    <option value="19.30">7.30 PM</option> 
                                                                                    <option value="20.00">8.00 PM</option> 
                                                                                    <option value="20.30">8.30 PM</option> 
                                                                                </select>
                                                                            </div>   
                                                                            <?php if(isset($_SESSION['start_timeErrMsg']) && !empty($_SESSION['start_timeErrMsg'])){ ?>

                                                                                <div class="mb-15">
                                                                                    <?php echo $_SESSION['start_timeErrMsg'];
                                                                                    unset($_SESSION['start_timeErrMsg']) ?>
                                                                                </div>

                                                                                <?php } ?>                                                                                                                    
                                                                            </div>

                                                                            <div class="col-md-6">
                                                                                <div class="mb-15">
                                                                                    <select name="ending_time" data-placeholder="End Time" class="select">
                                                                                        <option></option>
                                                                                        <option selected value="<?php echo $data['ending_time'] ?>">
                                                                                            <?php 
                                                                                            if($data['ending_time'] <='12.00'){
                                                                                                echo $data['ending_time'].' AM';
                                                                                            }else{
                                                                                                $etime = $data['ending_time']-'12.00';
                                                                                                echo number_format($etime,2,'.','').' PM';
                                                                                            } 
                                                                                            ?></option>
                                                                                            <option value="10.00">10.00 AM</option> 
                                                                                            <option value="10.30">10.30 AM</option> 
                                                                                            <option value="11.00">11.00 AM</option> 
                                                                                            <option value="11.30">11.30 AM</option> 
                                                                                            <option value="12.00">12.00 PM</option> 
                                                                                            <option value="12.30">12.30 PM</option> 
                                                                                            <option value="13.00">1.00 PM</option> 
                                                                                            <option value="13.30">1.30 PM</option> 
                                                                                            <option value="14.00">2.00 PM</option> 
                                                                                            <option value="14.30">2.30 PM</option> 
                                                                                            <option value="15.00">3.00 PM</option> 
                                                                                            <option value="15.30">3.30 PM</option> 
                                                                                            <option value="16.00">4.00 PM</option> 
                                                                                            <option value="16.30">4.30 PM</option> 
                                                                                            <option value="17.00">5.00 PM</option> 
                                                                                            <option value="17.30">5.30 PM</option> 
                                                                                            <option value="18.00">6.00 PM</option> 
                                                                                            <option value="18.30">6.30 PM</option> 
                                                                                            <option value="19.00">7.00 PM</option> 
                                                                                            <option value="19.30">7.30 PM</option> 
                                                                                            <option value="20.00">8.00 PM</option> 
                                                                                            <option value="20.30">8.30 PM</option> 
                                                                                            <option value="21.00">9.00 PM</option> 
                                                                                        </select>
                                                                                    </div>

                                                                                    <?php if(isset($_SESSION['ending_timeErrMsg']) && !empty($_SESSION['ending_timeErrMsg'])){ ?>

                                                                                        <div class="mb-15">
                                                                                            <?php echo $_SESSION['ending_timeErrMsg'];
                                                                                            unset($_SESSION['ending_timeErrMsg']) ?>
                                                                                        </div>

                                                                                        <?php } ?>
                                                                                    </div>








                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </fieldset>
                                                                </div>
                                                            </div>

                                                            <div class="text-right">
                                                                <button type="submit" name="updatedCourse" class="btn btn-primary">Update Course <i class="icon-arrow-right14 position-right"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>


                                                <script type="text/javascript">
                                                    $('.pickadate-selectors').pickadate();
                                                </script>

                                                <?php }else{ 
                                                    echo '<script type="text/javascript">location.replace("?p=404");</script>';
                                                } ?>
