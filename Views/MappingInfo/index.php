<?php 
//$labInfoObject = new Apps\LabInfo\LabInfo();
$assignedCourses = $MappingInfoObject->index();


if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){
    echo '<h3>'.$_SESSION['msg'].'</h3>';
    unset($_SESSION['msg']);
}

?>

<div class="breadcrumb-line">
    <ul class="breadcrumb">
        <li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">All Assaign Courses</li>
    </ul>
</div>
<br>
<div class="tab-pane">
    <a href="?p=assignCourse">
        <button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Assign New Course</button>
    </a> 
</div>
<br>
<!-- extra menu link -->

<?php if(!empty($assignedCourses)){ ?>
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h4 class="panel-title">All Assaign Courses Information</h4>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>                
                </ul>
            </div>
        </div>


        <table class="table datatable-basic">
            <thead>
                <tr>
                    <th>Course Name</th>
                    <th>Batch No</th>
                    <th>Lab No</th>
                    <th>Day</th>
                    <th>Time</th>
                    <th colspan="3" class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php 

                foreach ($assignedCourses as $item) { ?>

                    <tr>
                        <td><?php 
                            $_REQUEST['courseName']=$item['course_id'];
                            $getCourseName = $courseInfoObject->assign($_REQUEST)->getCourseName();
                            $cn = $getCourseName['title'];
                            echo '<a href="?p=courseDetails&id='.$item['course_id'].'">'.$cn.'</a>'; ?>

                        </td>
                        <td><?php 
                            $bn = $item['batch_no'];
                            echo '<a href="" title="Click here to get Batch Info">Batch-'.$bn.'</a>' ?></td>
                            <td><?php 
                                $ln = $item['lab_id'];
                                echo '<a href="" title="Click here to get Lab Schedule">'.$ln.'</a>' ?></td>
                                <td><?php if($item['day'] == 1){
                                    echo 'Friday';
                                }elseif ($item['day'] == 2) {
                                    echo 'Sat-Mon-Wed';
                                }elseif ($item['day'] = 3) {
                                    echo 'Sun-Tue-Thu';
                                }else{
                                    echo 'NA';
                                } ?></td>
                                <td><?php 
                                    if($item['start_time']<= '12.00'){
                                        echo $item['start_time'].' AM';
                                    }else{
                                        $stime = $item['start_time']-'12.00';  

                                        echo number_format($stime, 2, '.', '').' PM';  
                                    }
                                    echo ' - ';
                                    if($item['ending_time'] <='12.00'){
                                        echo $item['ending_time'].' AM';
                                    }else{
                                        $etime = $item['ending_time']-'12.00';
                                        echo number_format($etime,2,'.','').' PM';
                                    }      

                                    ?></td>
                                    <td class="text-center">


                                        <a href="?p=courseDetails&id=<?php echo $item['course_id'] ?>">
                                            <button type="button" class="btn bg-green-800 btn-labeled btn-rounded"><b><i class="icon-grid"></i></b>Details
                                            </button>
                                        </a>                            
                                        <a href="?p=editassignedCourse&id=<?php echo $item['course_id'] ?>">
                                            <button type="button" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-pencil"></i></b>Edit
                                            </button>
                                        </a>                            
                                        <a href="?p=deleteassignedCourse&id=<?php echo $item['id'] ?>">
                                            <button onclick="return delCheck();" type="button" class="btn bg-danger-400 btn-labeled btn-rounded"><b><i class="icon-trash"></i></b>Delete
                                            </button>
                                        </a>                        
                                    </td>
                                </tr>



                                <?php } ?>



                            </tbody>
                        </table>
                    </div>
                    <!-- /basic datatable -->
                    <script  type="text/javascript" >
                        function delCheck(){
                            return confirm('Are You Sure to Delete ??');
                        }
                    </script>



                    <?php }else{ ?>
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h4 class="panel-title">There are no Assaign Courses Information</h4>
                                <div class="heading-elements">
                                    <ul class="icons-list">
                                        <li><a data-action="collapse"></a></li>                
                                    </ul>
                                </div>
                            </div>
                        </div>

                            <?php } ?>



