<?php
// echo '<pre>';
// print_r($_REQUEST['day']);

// $data = $MappingInfoObject->assign($_REQUEST)->leadTrainerShedule();

// print_r($data);

// if($data == 1){
//     echo '<script>alert("Lead Trainer Busy")</script>';
// }else{
//     echo '<script>alert("Lead Trainer Free")</script>';    
// }


if (isset($_REQUEST['assignCourse'])) {

    //$chkLabno = $labInfoObject->assign($_REQUEST)->chkLabNo();
    $chkLeadTrainerShedule = $MappingInfoObject->assign($_REQUEST)->leadTrainerShedule();
    $chkassistTrainerShedule = $MappingInfoObject->assign($_REQUEST)->assistTrainerShedule();
    $chklabAsstShedule = $MappingInfoObject->assign($_REQUEST)->labAsstShedule();
    $chklabShedule = $MappingInfoObject->assign($_REQUEST)->labShedule();
    $chkBatchno = $MappingInfoObject->assign($_REQUEST)->chkBatchno();
    $errors = array();

    // course name 
    if (empty($_REQUEST['courseName'])) {
        $errors[] = 'Course Name Required...';
        $_SESSION['courseNameErrMsg'] = '<font color="red">Course Name Required...</font>';
    }

    // labno

    if (empty($_REQUEST['labNo'])) {
        $errors[] = 'Lab No Required...';
        $_SESSION['labNoErrMsg'] = '<font color="red">Lab No Required...</font>';
    }elseif ($chklabShedule > 0) {
        $errors[] = 'Lab BUSY...';
        $_SESSION['labNoErrMsg'] = '<font color="red">Lab BUSY...</font> <a href="?p=assigned" title="">Lab Schedule ?</a>';
    }  else {
        $_SESSION['labNoValue'] = $_REQUEST['labNo'];
    }

    // lead trainer

    if (empty($_REQUEST['lead_trainer'])) {
        $errors[] = 'Lead Trainer Required...';
        $_SESSION['lead_trainerErrMsg'] = '<font color="red">Lead Trainer Required...</font>';
    }elseif($chkLeadTrainerShedule > 0) {
        $errors[] = 'Lead Trainer BUSY...';
        $_SESSION['lead_trainerErrMsg'] = '<font color="red">Lead Trainer BUSY...</font>';
    }
    else {
        $_SESSION['lead_trainerValue'] = $_REQUEST['lead_trainer'];
    }

    // assistant trainer

    if (empty($_REQUEST['asst_trainer'])) {
        $errors[] = 'Assistant Trainer Required...';
        $_SESSION['asst_trainerErrMsg'] = '<font color="red">Assistant Trainer Required...</font>';
    }elseif ($chkassistTrainerShedule > 0) {
        $errors[] = 'Assistant Trainer BUSY...';
        $_SESSION['asst_trainerErrMsg'] = '<font color="red">Assistant Trainer BUSY...</font>';
    } else {
        $_SESSION['asst_trainerValue'] = $_REQUEST['asst_trainer'];
    }

    // lab assistant

    if (empty($_REQUEST['lab_asst'])) {
        $errors[] = 'Lab Assistant Required...';
        $_SESSION['lab_asstErrMsg'] = '<font color="red">Lab Assistant Required...</font>';
    }elseif ($chklabAsstShedule > 0) {
        $errors[] = 'Lab Assistant BUSY...';
        $_SESSION['lab_asstErrMsg'] = '<font color="red">Lab Assistant BUSY...</font>';
    } else {
        $_SESSION['lab_asstValue'] = $_REQUEST['lab_asst'];
    }

// is friday
    if (empty($_REQUEST['day'])) {
        if($_REQUEST['day'] == '1'){
         if (empty($_REQUEST['batch_no'])) {
            $errors[] = 'Batch No Required...';
            $_SESSION['batch_noErrMsg'] = '<font color="red">Batch No Required...</font>';
        } else {
            $_SESSION['batch_noValue'] = $_REQUEST['batch_no'];
        }

    }else{
        if (empty($_REQUEST['batch_no'])) {
            $errors[] = 'Batch No Required...';
            $_SESSION['batch_noErrMsg'] = '<font color="red">Batch No Required...</font>';
        }elseif ($chkBatchno > 0) {
            $errors['batch'] = 'Batch No Already Exist...';
            $_SESSION['batch_noErrMsg'] = '<font color="red">Batch No Already Exist...</font>';
        } else {
            $_SESSION['batch_noValue'] = $_REQUEST['batch_no'];
        }
    }
}

    // batch_no



    // start_date
if (empty($_REQUEST['start_date'])) {
    $errors[] = 'Start Date Required...';
    $_SESSION['start_dateErrMsg'] = '<font color="red">Start Date Required...</font>';
} else {
    $_SESSION['start_dateValue'] = $_REQUEST['start_date'];
}

    // ending_date
if (empty($_REQUEST['ending_date'])) {
    $errors[] = 'End Date Required...';
    $_SESSION['ending_dateErrMsg'] = '<font color="red">End Date Required...</font>';
} else {
    $_SESSION['ending_dateValue'] = $_REQUEST['ending_date'];
}

    //chk day mismatch
if(!empty($_REQUEST['start_date']) && !empty($_REQUEST['ending_date'])){

    $start_date = $_REQUEST['start_date'];
    $start_date = date("Y-m-d", strtotime($start_date));

    $ending_date = $_REQUEST['ending_date'];
    $ending_date = date("Y-m-d", strtotime($ending_date));

    if($start_date > $ending_date){
        $errors[] = 'End Date Smaller Than Start Date...';
        $_SESSION['ending_dateErrMsg'] = '<font color="red">End Date Smaller Than Start Date......</font>';
    }
}




    //firstDay

if (empty($_REQUEST['day'])) {
    $errors[] = 'Day Required...';
    $_SESSION['dayErrMsg'] = '<font color="red">Day Required...</font>';
} else {
    $_SESSION['dayValue'] = $_REQUEST['day'];
}




    //start_time
if (empty($_REQUEST['start_time'])) {
    $errors[] = 'Start Time Required...';
    $_SESSION['start_timeErrMsg'] = '<font color="red">Start Time Required...</font>';
} else {
    $_SESSION['start_timeValue'] = $_REQUEST['start_time'];
}

    //ending_time

if (empty($_REQUEST['ending_time'])) {
    $errors[] = 'End Time Required...';
    $_SESSION['ending_timeErrMsg'] = '<font color="red">End Time Required...</font>';
} else {
    $_SESSION['ending_timeValue'] = $_REQUEST['ending_time'];
}


if (empty($errors) == true) {
    $data = $MappingInfoObject->assign($_REQUEST)->store();

    if ($data == 1) {
        $_SESSION['msg'] = '<font color="green">Assaign a Course Successfully';
        echo '<script type="text/javascript">location.replace("?p=assigned");</script>';
    } else {
        echo $_SESSION['msg'] = "Error While Lab info Submitted";
           // echo '<script type="text/javascript">location.replace("?p=addLab");</script>';
    }
} else {
    echo '<script type="text/javascript">location.replace("?p=assignCourse");</script>';        
}
} else {

   echo '<script type="text/javascript">location.replace("?p=404");</script>';
}
?>