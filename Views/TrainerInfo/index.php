<?php 
$allTrainer = $TrainerInfoObject->index() ;
?>
<div class="breadcrumb-line">
	<ul class="breadcrumb">
		<li><a href="index.php"><i class="icon-home2 position-left"></i> Home</a></li>
		<li class="active">All Trainer</li>
	</ul>
</div>
<br>

<div class="tab-pane">
	<a href="?p=addTrainer">
		<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-plus3 position-left"></i> Add New Trainer</button>
	</a> 
	<a href="?p=addTrainer">
		<button type="button" class="btn border-slate text-slate-800 btn-flat"><i class="icon-search4 position-left"></i>Search Trainer</button>
	</a> 
</div>
<br>

<?php 
if(!empty($allTrainer)){ ?>
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">All Trainer's Info</h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>

				</ul>
			</div>
		</div>

		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>Photo</th>
					<th>Name</th>
					<th>Team Name</th>					
					<th>Trainer Level</th>
					<th>Course Name</th>
					<th colspan="3" class="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($allTrainer as $item) { ?>

					<tr>
						<td>
							<div class="media">
								<div class="media-left">
									<a href="assets/images/placeholder.jpg" data-popup="lightbox">
										<img src="assets/images/placeholder.jpg" class="img-circle img-lg" alt="">
									</a>
								</div>
							</div>
						</td>
							<td><a href="?p=trainerDetail&id=<?php echo $item['unique_id'] ?>">
								<?php echo ucwords($item['full_name']) ?></a> 
							</td>
							<td><?php echo ucwords($item['team']) ?></td>
							<td><?php 
								if($item['trainer_level'] == 1){
									echo 'Lead Trainer';
								}elseif ($item['trainer_level'] == 2) {
									echo 'Assistant Trainer';
								}elseif ($item['trainer_level'] == 3) {
									echo 'Lab Assistant';
								}else{
									echo 'NA';
								}
								?></td>

								<td><?php 
									$_REQUEST['courseName'] = $item['course_name'];
									$courseName = $courseInfoObject->assign($_REQUEST)->getCourseName();
									echo $courseName['title'] ?></td>
									<td class="text-center">


										<a href="?p=trainerDetail&id=<?php echo $item['unique_id'] ?>">
											<button type="button" class="btn bg-green-800 btn-labeled btn-rounded"><b><i class="icon-grid"></i></b>Details
											</button>
										</a>                            
										<a href="?p=editTrainer&id=<?php echo $item['unique_id'] ?>">
											<button disabled type="button" class="btn bg-teal-400 btn-labeled btn-rounded"><b><i class="icon-pencil"></i></b>Edit
											</button>
										</a>                            
										<a href="?p=deleteTrainer&id=<?php echo $item['unique_id'] ?>">
											<button disabled onclick="return delCheck();" type="button" class="btn bg-danger-400 btn-labeled btn-rounded"><b><i class="icon-trash"></i></b>Delete
											</button>
										</a>                        
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->
					<script  type="text/javascript" >
						function delCheck(){
							return confirm('Are You Sure to Delete ??');
						}
					</script>

					<?php }else{ ?>
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">There are no  Lab </h5>
								<div class="heading-elements">
									<ul class="icons-list">
										<li><a data-action="collapse"></a></li>

									</ul>
								</div>
							</div>
							<?php } ?>

