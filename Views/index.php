<?php 
include_once ('../vendor/autoload.php'); 
$labInfoObject = new Apps\LabInfo\LabInfo();
$MappingInfoObject = new Apps\MappingInfo\MappingInfo();
$TrainerInfoObject = new Apps\TrainerInfo\TrainerInfo();
$courseInfoObject = new Apps\CourseInfo\CourseInfo();

if(!isset($_SESSION['loginUser']) && empty($_SESSION['loginUser'])){
    echo '<script type="text/javascript">location.replace("../index.php");</script>';
}

include_once('layout/header.php'); 





?>
<?php
if (isset($_GET['p'])) {
    $page = $_GET['p'];
} else {
    $page = "";
}

switch ($page) {

    case 'addLab':
    require_once('LabInfo/create.php');
    break;
    case 'labStore':
    require_once('LabInfo/store.php');
    break;
    case 'allLab':
    require_once('LabInfo/index.php');
    break;
    case 'labShow':
    require_once('LabInfo/show.php');
    break;
    case 'editLab':
    require_once('LabInfo/edit.php');
    break;
    case 'labUpdate':
    require_once('LabInfo/update.php');
    break;
    case 'deleteLab':
    require_once('LabInfo/delete.php');
    break;
    case 'searchLab':
    require_once('LabInfo/search.php');
    break;
    case '404':
    require_once('LabInfo/error_404.php');
    break;

    case 'assignCourse':
    require_once('MappingInfo/create.php');
    break;
    case 'assignStore':
    require_once('MappingInfo/store.php');
    break;
    case 'assigned':
    require_once('MappingInfo/index.php');
    break;
    case 'courseDetails':
    require_once('MappingInfo/courseDetails.php');
    break;
    case 'editassignedCourse':
    require_once('MappingInfo/edit.php');
    break;    
    case 'updateAssaignCourses':
    require_once('MappingInfo/update.php');
    break;
    case 'deleteassignedCourse':
    require_once('MappingInfo/delete.php');
    break;

    case 'addCourse':
    require_once('CourseInfo/create.php');
    break;
    case 'cDetails':
    require_once('CourseInfo/show.php');
    break;
    case 'allCourses':
    require_once('CourseInfo/index.php');
    break;

    case 'allSoft':
    require_once('SoftwareInfo/index.php');
    break;

    case 'allTrainer':
    require_once('TrainerInfo/index.php');
    break;
    case 'trainerDetail':
    require_once('TrainerInfo/show.php');
    break;




    // case 'login':
    // require_once('UserInfo/login.php');
    // break;

    // case 'loginPro':
    // require_once('UserInfo/loginProcess.php');
    // break;
    // case 'logout':
    // require_once('UserInfo/logout.php');
    // break;


    
    
    


    default:

    require_once('layout/dashboard.php');
    break;
}
?>
<!-- /2 columns form -->


<!-- /end content here -->
<?php include_once('layout/footer.php'); ?>
