<?php 
$todaysClass = $MappingInfoObject->todaysClass();

?>
<div class="row">
	
	<!-- Quick stats boxes -->
	<div class="row">

		<div class="col-md-3">

			<!-- Total Courses -->
			<a href="?p=allCourses" title="">
				<div class="panel bg-purple-600">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-10">
								<div class="heading-elements">
									<span><i class="icon-server text-white-400 counter-icon"></i></span>
								</div>

								<h3 class="no-margin"><?php $totalCourse = $courseInfoObject->getTotalCourse();
									print_r($totalCourse); ?></h3>
									Total Courses
								</div>
							</div>
						</div>
						<!-- /Total Courses -->
					</div>
				</a>
			</div>

			<!-- Running Courses -->
			<div class="col-md-3">
				<a href="?p=assigned" title="">
					<div class="panel bg-teal-400">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-10">
									<div class="heading-elements">
										<span><i class="icon-rocket text-white-400 counter-icon"></i></span>
									</div>

									<h3 class="no-margin">
										<?php $getRunningCourses = $MappingInfoObject->getRunningCourses();
										print_r($getRunningCourses); ?>

									</h3>
									Running Courses

								</div>
							</div>
						</div>

					</div>
				</a>

			</div>
			<!-- /Running Courses -->

			<!-- Lab -->
			<div class="col-md-3">

				<a href="?p=allLab" title="">
					<div class="panel bg-pink-400">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-10">
									<div class="heading-elements">
										<span><i class="icon-display text-white-400 counter-icon"></i></span>
									</div>

									<h3 class="no-margin">
										<?php $getTotalLabNumber = $labInfoObject->getTotalLabNumber();
										print_r($getTotalLabNumber); ?>
									</h3>
									Digital Lab
								</div>							
							</div>
						</div>

					</div>
				</a>
			</div>
			<!-- /Lab -->

			<!-- Trainer -->
			<div class="col-md-3">


				<a href="?p=allTrainer" title="">
					<div class="panel bg-blue-600">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-10">
									<div class="heading-elements">
										<span><i class="icon-users4 text-white-400 counter-icon"></i></span>
									</div>

									<h3 class="no-margin">
										<?php $getTotalTrainerNumber = $TrainerInfoObject->getTotalTrainerNumber();
										print_r($getTotalTrainerNumber); ?>
									</h3>
									Total Trainers
								</div>
							</div>
						</div>

					</div>

				</a>
			</div>
			<!-- /Trainer -->
		</div>


		<div class="row">
			<div class="col-lg-6">
				<!-- Daily sales -->
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h6 class="panel-title">Today's Class Schedule</h6>
						<div class="heading-elements">
							<ul class="icons-list">
								<li><a data-action="collapse"></a></li>
							</ul>
						</div>
					</div>
					<?php 
					if(!empty($todaysClass)){ ?>
						<div class="table-responsive">
							<table class="table text-nowrap">
								<thead>
									<tr>
										<th>Trainer's Name</th>
										<th>Course Name</th>
										<th>Time</th>
										<th>Lab</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($todaysClass as $value) { 
										$_REQUEST['courseName']=$value['course_id'];
										$getCourseName = $courseInfoObject->assign($_REQUEST)->getCourseName();;

										$_REQUEST['id'] = $value['lead_trainer'];
										$leadTrainerName = $TrainerInfoObject->assign($_REQUEST)->leadTrainerName();
										$_REQUEST['id'] = $value['asst_trainer'];
										$assistantTrainerName = $TrainerInfoObject->assign($_REQUEST)->assistantTrainerName();
										$_REQUEST['id'] = $value['lab_asst'];
										$labAssistantName = $TrainerInfoObject->assign($_REQUEST)->labAssistantName();?>
										<tr>
											<td>
												<div class="media-left media-middle">
													<a href="#" class="btn bg-primary-400 btn-rounded btn-icon btn-xs">
														<span class="letter-icon"></span>
													</a>
												</div>

												<div class="media-body">
													<div class="media-heading">
														<a href="#" class="letter-icon-title">
															<?php echo ucwords($leadTrainerName['full_name']); ?><br>
															<?php echo ucwords($assistantTrainerName['full_name']); ?><br>
															<?php echo ucwords($labAssistantName['full_name']); ?><br>
														</a>
													</div>
												</div>
											</td>
											<td>
												<div class="text-muted text-size-small"> 
													<?php echo ucwords($getCourseName['title']); ?>													
												</div>
											</td>
											<td>
												<span class="text-muted text-size-small">
													<?php
													if($value['start_time']<= '12.00'){
														echo $value['start_time'].' AM';
													}else{
														$stime = $value['start_time']-'12.00';  

														echo number_format($stime, 2, '.', '').' PM';  
													} ?>
													<br>
													<?php
													if($value['ending_time']<= '12.00'){
														echo $value['ending_time'].' AM';
													}else{
														$stime = $value['ending_time']-'12.00';  

														echo number_format($stime, 2, '.', '').' PM';  
													} ?>
												</span>
											</td>
											<td>
												<h6 class="no-margin">
													<?php echo $value['lab_id'] ?>
												</h6>
											</td>
										</tr>
										<?php }?>


									</tbody>
								</table>
							</div>
							<?php }else{
								echo 'data nai';
							}
							?>

						</div>


					</div>

					<div class="col-lg-6">
				<!-- Daily sales -->
				


					</div>

		</div>

