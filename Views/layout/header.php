<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BITM LabMS</title>

    <!-- Global stylesheets -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
    <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/nicescroll.min.js"></script>

    <!-- for button -->
    <script type="text/javascript" src="assets/js/plugins/velocity/velocity.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/velocity/velocity.ui.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/buttons/spin.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/buttons/ladda.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/components_buttons.js"></script>
    <!-- /for button -->
    <!-- for form  -->
    <script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/form_layouts.js"></script>
    <!-- /for form  -->
    <!-- notify -->
    <script type="text/javascript" src="assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/components_notifications_pnotify.js"></script>
    <!-- /notify -->
    <script type="text/javascript" src="assets/js/pages/picker_date.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/jquery_ui/effects.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/anytime.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.date.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.time.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/pickadate/legacy.js"></script>
    
    <!-- for image -->
    <script type="text/javascript" src="assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/user_pages_team.js"></script>

    <!-- for data table -->
    <script type="text/javascript" src="assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/tables/datatables/extensions/tools.min.js"></script>
    <script type="text/javascript" src="assets/js/pages/datatables_extension_tools.js"></script>

    <!-- <script type="text/javascript" src="assets/js/pages/datatables_basic.js"></script> -->

    <!--for data table-->
    <script type="text/javascript" src="assets/js/core/app.js"></script>
    <script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
    <script type="text/javascript" src="assets/js/pages/layout_fixed_custom.js"></script>
    <!-- /theme JS files -->


</head>

<body class="navbar-top">

    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php"><img src="assets/images/logo_light.png" alt=""></a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">				


                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="assets/images/placeholder.jpg" alt="">
                        <span>BITM</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                        <li><a href="UserInfo/logout.php"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content">

                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="category-content">
                            <div class="media">
                                <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                <div class="media-body">
                                    <span class="media-heading text-semibold">BITM LabMS</span>
                                    <div class="text-size-mini text-muted">
                                        <i class="icon-pin text-size-small"></i> &nbsp;Karwanbazar, Dhaka
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->


                    <!-- Main navigation -->
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">

                                <li  class="active"><a href="index.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>

                                <li>
                                    <a href="#"><i class="icon-user-plus"></i> <span>User Management</span></a>
                                    <ul>
                                        <li><a href="#">Add User</a></li>
                                        <li><a href="#">All User</a></li>
                                        <li><a href="#">Search User</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#"><i class="icon-people"></i> <span>Trainer Management</span></a>
                                    <ul>
                                        <li><a href="#">Add Trainer</a></li>
                                        <li><a href="?p=allTrainer">All Trainer</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href=""><i class="icon-copy"></i> <span>Course Info</span></a>
                                    <ul>
                                        <li><a href="?p=addCourse">
                                            Add Course</a></li>
                                            <li><a href="?p=allCourses">All Course</a></li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="#"><i class="icon-task"></i> <span>Lab Info</span></a>
                                        <ul>
                                            <li><a href="?p=addLab">
                                                Add Lab</a></li>
                                                <li ><a href="?p=allLab">All Lab</a></li>
                                            </ul>
                                        </li>


                                        <li>
                                            <a href="#"><i class="icon-lifebuoy"></i> <span>Install Software Info</span></a>
                                            <ul>
                                                <li><a href="#">Add Software</a></li>
                                                <li><a href="?p=allSoft">All Software</a></li>
                                                <li><a href="#">Search Software</a></li>
                                            </ul>
                                        </li>


                                        <li>
                                            <a id="robi" href="#"><i class="icon-map5"></i> <span>Lab Management</span></a>
                                            <ul id="robi">										
                                                <li ><a href="?p=assignCourse">Add Course into Lab</a></li>
                                                <li><a href="?p=assigned">All Assign Courses</a></li>
                                            </ul>
                                        </li>


                                    </ul>
                                </div>
                            </div>
                            <!-- /main navigation -->

                        </div>
                    </div>
                    <!-- /main sidebar -->


                    <!-- Main content -->
                    <div class="content-wrapper">
                        <!-- Content area -->
                        <div class="content">

