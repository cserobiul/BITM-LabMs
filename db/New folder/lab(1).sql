-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 11, 2016 at 07:45 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lab`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_active` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_active`, `created`, `updated`, `deleted`) VALUES
(1, '57906272d0888', 'Web App Development', '3 month,144 hours', 'it''s basically php course', 'free', '00', 1, '2016-08-08 02:08:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '346324685676h7', 'Computer IT Support', '3 month, 144 hours', 'hmm', 'free', '00', 1, '2016-08-08 00:04:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '84832sd8384d6d', 'Core java', '90 Days, 150 Hours', 'Object-oriented Programming\r\n\r\n• Define modeling concepts: abstraction, encapsulation, and packages\r\n\r\n• Discuss Java technology application code reuse\r\n\r\n• Define class, member, attribute, method, constructor, and package\r\n\r\n• Use the access modifiers private and public as appropriate for the guidelines of encapsulation\r\n\r\n• Invoke a method on a particular object\r\n\r\n• Use the Java technology API online documentation', 'paid', '15000', 1, '2016-08-08 02:06:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '57a677689eaf6', 'Graphics Design', '90 days, 150 hours', 'hmm', 'free', '00', 1, '2016-08-09 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE IF NOT EXISTS `course_trainer_lab_mapping` (
  `id` int(11) NOT NULL,
  `course_id` varchar(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` varchar(111) NOT NULL,
  `start_date` date NOT NULL,
  `ending_date` date NOT NULL,
  `start_time` varchar(111) NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_trainer_lab_mapping`
--

INSERT INTO `course_trainer_lab_mapping` (`id`, `course_id`, `batch_no`, `lead_trainer`, `asst_trainer`, `lab_asst`, `lab_id`, `start_date`, `ending_date`, `start_time`, `ending_time`, `day`, `assigned_by`, `created`, `updated`, `deleted`) VALUES
(1, '57906272d0888', '28', '78d7s87d8sds', '93c9394777', 'd7f8d78f7d8', '402', '2016-05-16', '2016-08-14', '18.00', '21.00', 'Sunday', 'arif', '2016-08-02 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '84832sd8384d6d', '32', '8dfd8f8d7f87d8', 'c7834838v838h', 'd8fd8f8d6f6d', '404', '2016-05-17', '2016-08-14', '13.30', '17.30', 'Sunday', 'arif', '2016-08-09 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '57906272d0888', '27', '78d7s87d8sds', '93c9394777', 'd7f8d78f7d8', '402', '2016-05-16', '2016-08-15', '13.30', '17.30', 'Sunday', 'arif', '2016-05-11 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '57a677689eaf6', '29', 'f8sd78f7sd87f8', 'c7834838v838h', 'd8fd8f8d6f6d', '403', '2016-05-14', '2016-08-14', '8.00', '11.00', 'Sunday', 'arif', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE IF NOT EXISTS `installed_softwares` (
  `id` int(11) NOT NULL,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE IF NOT EXISTS `labinfo` (
  `id` int(11) NOT NULL,
  `course_id` varchar(100) NOT NULL,
  `lab_no` varchar(111) NOT NULL,
  `seat_capacity` varchar(111) NOT NULL,
  `projector_resolution` varchar(111) NOT NULL,
  `ac_status` varchar(111) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc_configuration` varchar(255) NOT NULL,
  `table_capacity` varchar(100) NOT NULL,
  `internet_speed` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labinfo`
--

INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc_configuration`, `table_capacity`, `internet_speed`, `created`, `updated`, `deleted`) VALUES
(16, '57a677689eaf6', '405', '30', '1024x768', '1', 'a:4:{s:9:"processor";s:10:"Core 2 Due";s:3:"ram";s:4:"2 gb";s:5:"brand";s:4:"Dell";s:2:"os";s:11:"Windows 8.1";}', '', 'a:4:{s:9:"processor";s:7:"Core I5";s:3:"ram";s:4:"4 gb";s:5:"brand";s:4:"Dell";s:2:"os";s:3:"Mac";}', '20', '5 MB', '2016-08-10 09:43:25', '2016-08-10 14:58:51', '0000-00-00 00:00:00'),
(17, '57906272d0888', '402', '35', '1024x768', '2', 'a:4:{s:9:"processor";s:7:"Core I5";s:3:"ram";s:4:"2 gb";s:5:"brand";s:4:"Dell";s:2:"os";s:11:"Windows 8.1";}', '', 'a:4:{s:9:"processor";s:7:"Core I7";s:3:"ram";s:4:"8 gb";s:5:"brand";s:7:"Sumsang";s:2:"os";s:3:"Mac";}', '15', '2 MB', '2016-08-10 15:01:32', '2016-08-10 15:02:26', '0000-00-00 00:00:00'),
(18, '346324685676h7', '401', '32', '1024x768', '2', 'a:4:{s:9:"processor";s:10:"core 2 due";s:3:"ram";s:4:"2 gb";s:5:"brand";s:7:"sumsang";s:2:"os";s:9:"windows 8";}', '', 'a:4:{s:9:"processor";s:7:"celeron";s:3:"ram";s:4:"4 gb";s:5:"brand";s:7:"sumsang";s:2:"os";s:3:"mac";}', '20', '1 MB', '2016-08-11 05:33:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, '84832sd8384d6d', '403', '30', '1024x768', '3', 'a:4:{s:9:"processor";s:11:"core 2 quad";s:3:"ram";s:4:"2 gb";s:5:"brand";s:7:"sumsang";s:2:"os";s:9:"windows 8";}', '', 'a:4:{s:9:"processor";s:7:"celeron";s:3:"ram";s:4:"2 gb";s:5:"brand";s:4:"dell";s:2:"os";s:3:"mac";}', '15', '2 MB', '2016-08-11 05:35:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, '57906272d0888', '404', '30', '1024x768', '4', 'a:4:{s:9:"processor";s:7:"core i5";s:3:"ram";s:4:"4 gb";s:5:"brand";s:7:"sumsang";s:2:"os";s:10:"windows 10";}', '', 'a:4:{s:9:"processor";s:7:"core i7";s:3:"ram";s:4:"8 gb";s:5:"brand";N;s:2:"os";s:3:"mac";}', '18', '2 MB', '2016-08-11 05:35:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE IF NOT EXISTS `trainers` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(111) NOT NULL,
  `course_name` varchar(111) NOT NULL,
  `trainer_level` varchar(111) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(111) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `web` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `course_name`, `trainer_level`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`, `updated`, `deleted`) VALUES
(1, '78d7s87d8sds', 'Jadid sir', 'm.sc in cse', 'code breaker', '57906272d0888', '1', '', '01890098909', 'jadid@gmail.com', 'dhaka', 'male', 'hmm.com', '2016-08-09 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '93c9394777', 'sumon mahmud', 'b.sc in cse', 'code breaker', '57906272d0888', '2', '', '01909909009', 'sumon@gmail.com', 'dhaka', 'male', 'hmm2.com', '2016-08-09 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'd7f8d78f7d8', 'sayem morshed', 'diploma in computer', 'code breaker', '57906272d0888', '3', '', '01000000000', 'sayem@gmail.com', 'dhaka', 'male', 'hmm3.com', '2016-08-09 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'c7834838v838h', 'sahara khatun', 'b.sc in cse', 'div vill', '84832sd8384d6d', '2', '', '01909879098', 'sahara@gmail.com', 'dhaka', 'female', 'hmm4.com', '2016-08-10 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'f8sd78f7sd87f8', 'Madar Teresa', 'm.sc in cse', 'code breaker', '57906272d0888', '1', '', '01987000099', 'terasa@gmail.com', 'dhaka', 'female', 'hmm.com', '2016-08-09 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '8dfd8f8d7f87d8', 'Md Mustak', 'm.sc in cse', 'inigma', '84832sd8384d6d', '1', '', '01909909009', 'muskta@hotmail.com', 'dhaka', 'male', 'hmm.com', '2016-08-10 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'd8fd8f8d6f6d', 'Monika', 'diploma in computer', 'div vill', '84832sd8384d6d', '3', '', '01909000999', 'monika@gmail.com', 'dhaka', 'female', 'hmm.com', '2016-08-11 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`, `updated`, `deleted`) VALUES
(1, '57a67863e76dc', 'Ariful Haque', 'arif', 'arif@hotmail.com', 'aaa', '', 1, 1, 0, '2016-08-08 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labinfo`
--
ALTER TABLE `labinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `labinfo`
--
ALTER TABLE `labinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
